@extends('admin.panel.layouts.master')
@section('title','Контенты')
@section('content')
    <div class="ui segment ">
        <div class="ui  segment">
            <div class="ui breadcrumb ">
                <a class="section" href="/admin">Админ панель</a>
                <div class="divider"> /</div>
                <div class="active section">Контенты</div>
            </div>
        </div>
        <div id="contentsGrid"></div>
    </div>
@endsection
@push('js')
<script src="{{asset('assets/admin/js/content.js')}}" type="text/javascript"></script>
@endpush