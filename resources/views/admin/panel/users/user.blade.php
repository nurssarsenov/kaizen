@extends('admin.panel.layouts.master')
@section('title','Пользователи')
@section('content')
    <div class="ui segment ">
        <div class="ui  segment">
            <div class="ui breadcrumb ">
                <a class="section" href="/admin">Админ панель</a>
                <div class="divider"> /</div>
                <div class="active section">Пользователи</div>
            </div>
        </div>
        <div id="usersGrid"></div>
    </div>
@endsection
@push('js')
<script src="{{asset('assets/admin/js/user.js')}}" type="text/javascript"></script>
@endpush