@extends('admin.panel.layouts.master')
@section('title','курсы')
@section('content')
    <div class="ui segment ">
        <div class="ui  segment">
            <div class="ui breadcrumb ">
                <a class="section" href="/admin">Админ панель</a>
                <div class="divider"> /</div>
                <div class="active section">Курсы</div>
            </div>
        </div>
        <div id="coursesGrid"></div>
    </div>
@endsection
@push('js')
<script src="{{asset('assets/admin/js/course.js')}}" type="text/javascript"></script>
@endpush