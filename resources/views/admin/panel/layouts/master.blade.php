<!DOCTYPE html>
<html>
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon" />
@include('admin.includes.css')
@stack('css')
<!-- Site Properties -->
    <title>@yield('title')</title>
</head>
<body>
<div class="ui top attached demo menu">
    <a class="item" id="menu">
        <i class="sidebar icon"></i> Меню
    </a>
    <div class="right menu">
        <a class="ui item" href="/auth/logout">
            Выход
        </a>
    </div>
</div>
<div class="ui main  bottom attached segment">
    <div class="ui sidebar  vertical menu">
        @include('admin.includes.menu')
    </div>
    <div class="pusher">
        @yield('content')
    </div>
</div>
@include('admin.includes.js')
<script>
    $('.ui.sidebar').sidebar({
        context: $('.bottom.segment')
    })
        .sidebar('attach events', '.menu #menu');
</script>
@stack('js')
</body>
</html>
