@extends('admin.panel.layouts.master')
@section('title','База клиентов')
@section('content')
    <div class="ui segment ">
        <div class="ui  segment">
            <div class="ui breadcrumb ">
                <a class="section" href="/admin">Админ панель</a>
                <div class="divider"> /</div>
                <div class="active section">База клиентов</div>
            </div>
        </div>
        <div id="clientBaseGrid"></div>
    </div>
@endsection
@push('js')
<script src="{{asset('/assets/admin/js/client_base.js')}}" type="text/javascript"></script>
@endpush