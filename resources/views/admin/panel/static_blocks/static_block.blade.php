@extends('admin.panel.layouts.master')
@section('title','Статические блоки')
@section('content')
    <div class="ui segment ">
        <div class="ui  segment">
            <div class="ui breadcrumb ">
                <a class="section" href="/admin">Админ панель</a>
                <div class="divider"> /</div>
                <div class="active section">Статические блоки</div>
            </div>
        </div>
        <div id="staticBlocksGrid"></div>
    </div>
@endsection
@push('js')
<script src="{{asset('assets/admin/js/static_block.js')}}" type="text/javascript"></script>
@endpush