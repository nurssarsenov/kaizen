@extends('admin.panel.layouts.master')
@section('title','Админ панель')
@section('content')
    <div class="ui segment ">
        <div class="ui  segment">
            <div class="ui breadcrumb ">
                <a class="section" href="/admin">Админ панель</a>
                <div class="divider"> /</div>
                <div class="active section">Заказы</div>
            </div>
        </div>
        <div id="ordersGrid"></div>
    </div>
@endsection
@push('js')
<script src="{{asset('/assets/admin/js/orders.js?06092018')}}" type="text/javascript"></script>
@endpush