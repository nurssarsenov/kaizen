@extends('admin.panel.layouts.master')
@section('title','Блоки')
@section('content')
    <div class="ui segment ">
        <div class="ui  segment">
            <div class="ui breadcrumb ">
                <a class="section" href="/admin">Админ панель</a>
                <div class="divider"> /</div>
                <div class="active section">Блоки</div>
            </div>
        </div>
        <div id="blocksGrid"></div>
    </div>
@endsection
@push('js')
    <script src="{{asset('assets/admin/js/block.js')}}" type="text/javascript"></script>
@endpush