@extends('web.master')
@section('content')
    <div class="section_container">
        <div class="container">
            <div class="section_container__white p-5">
                <div class="ui middle aligned center aligned grid">
                    <div class="card column">
                        <p class="h4 text-center mt-5">Восстановление пароля</p>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form class="section_container__tab___content" id="resetPasswordForm">
                            <div class="ui form">
                                <div class="inline fields">
                                    <div class="sixteen wide field">
                                        <label>E-mail</label>
                                        <input id="authLogin" type="text" name="email" placeholder="E-mail">
                                    </div>
                                </div>
                                <div class="section_container__button">
                                    <button class="ui orange submit " type="button" onclick="Auth.reset();" style="background: #ff521e; color: #ffffff; border-radius: 18px;font-size: 15px; padding: 15px 40px;">
                                        Восстановить
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection