@extends('admin.auth.master')
@section('title','Admin login')
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <div class="ui message">
                    <h3>Админ панель</h3>
            </div>
            <form class="ui large form" id="authForm">
                <div class="ui stacked segment">
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input id="authLogin" type="text" name="email" placeholder="email">
                        </div>
                    </div>
                    <div class="field ">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input id="authPassword" type="password" name="password" placeholder="Пароль">
                        </div>
                    </div>
                    <div class="ui fluid large red  submit button" onclick="Auth.login();">Войти</div>
                </div>
            </form>
        </div>
    </div>
@endsection