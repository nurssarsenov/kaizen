@extends('web.master')
@section('content')
    <div class="section_container">

        <div class="ui container">
            <div class="section_container__white card">
                <p class="h4 text-center mt-5">Регистрация Аккаунта</p>
                <div class="section_container__tab">
                </div>
                <div class="section_container__tab___content">
                    <div class="ui form">
                        <form action="#" id="userRegisterForm" method="post">
                            {{ csrf_field() }}
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Ф.И.О</label>
                                    <input type="text" name="fio" id="fio" placeholder="Фамилия Имя Отчество">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Должность</label>
                                    <input type="text" name="position" id="position" placeholder="Должность">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Компания</label>
                                    <input type="text" name="company" id="company" placeholder="Компания">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Город</label>
                                    <input type="text" name="city" id="city" placeholder="Город">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>E-mail</label>
                                    <input type="text" name="email" id="email" placeholder="E-mail">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Телефон</label>
                                    <input type="text" name="phone" id="phone" data-js="input" placeholder="Телефон" autocomplete="tel-local">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Пароль</label>
                                    <input type="password" name="password" id="password" data-js="input" placeholder="Password">
                                </div>
                            </div>

                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Повторите пароль</label>
                                    <input type="password" name="password2" id="password2" data-js="input" placeholder="Repeat Password">
                                </div>
                            </div>
                            <div class="section_container__bottom">
                                <p><span>*</span> Нажимая кнопку «Зарегистрироваться», Вы даете группе компаний Kaizen
                                    <a href="#">согласие на обработку </a>своих персональных данных</p>
                            </div>
                            <div class="section_container__button">
                                <button class="ui orange submit " type="button" onclick="Register.userRegister();">
                                    ПРОДОЛЖИТЬ
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection