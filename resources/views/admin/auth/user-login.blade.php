@extends('web.master')
@section('content')
    <div class="section_container">

        <div class="container">
            <div class="section_container__white p-5">
                <div class="ui middle aligned center aligned grid">
                    <div class="card column">
                        <p class="h4 text-center mt-5">Вход</p>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form class="section_container__tab___content" id="authForm">
                            <div class="ui form">
                                <div class="inline fields">
                                    <div class="sixteen wide field">
                                        <label>E-mail</label>
                                        <input id="authLogin" type="email" class="form-control" placeholder="E-mail">
                                    </div>
                                </div>
                                <div class="inline fields">
                                    <div class="sixteen wide field">
                                        <label>Пароль</label>
                                        <input type="password" id="authPassword" class="form-control" placeholder="Password">
                                    </div>
                                </div>
                                <div class="m-2">
                                    <a class="mr-5" href="{{ url('/resetPassword') }}">Забыли пароль?</a>
                                    <a href="{{ url('/register') }}">Регистрация</a>
                                </div>
                                <div class="section_container__button">
                                    <button class="ui orange submit " type="button"  onclick="Auth.login();" style="background: #ff521e; color: #ffffff; border-radius: 18px;font-size: 15px; padding: 15px 40px;">
                                        Войти
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection