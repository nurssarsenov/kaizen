@extends('web.master')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="height: 400px">
            <div class="card">
                <?php session_start()?>
                @if(isset($_SESSION['isResendVerification']) && $_SESSION['isResendVerification'])
                <div class="card-header">{{ __('Повторное письмо было выслано на вашу почту!') }}</div>
                    <br>
                    <?php $_SESSION['isResendVerification'] = false ?>
                @endif
                <div class="card-header">{{ __('Активируйте свой аккаунт при помощи письма, который был отправлен на ваш почтовый ящик ' . auth()->user()->email) }}</div>

                <div class="card-body">
                    {{ __('До того как посмотреть историю своих заказов, вы должны активировать свой аккаунт с помощью ссылки, который был отправлен на вашу почту') }}
                    {{ __('Если вы не получили пиьмо ') }}, <a style="border-bottom: #0ea432" href="{{ route('verification.resend') }}">{{ __('нажмите сюда ') }}</a> {{__("для отправки повторного письма")}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
