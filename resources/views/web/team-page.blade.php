@extends('web.master')
@section('content')
    <div class="section_container">
        <div class="ui container">
            <div class="section_container__white">
				<div style="text-align: center;">
					<img src="/uploads/{{$command['img']}}" alt="" style="max-width: 300px;margin-top: 50px;">
					<p class="section_container__title">{{$command['title']}}</p>
				</div>
                <div class="section_container__tab___content" style="padding-top: 0px;">
                    <p class="command_column__content___text"><?=$command['content']?></p>
					@if(strlen($command['pdf_file']) > 0)
						<div style="text-align: center; margin-top: 15px;">
							<a href="/uploads/{{$command['pdf_file']}}" target="_blank">
								<button class="section_courses__content___column____btn" style="max-width: 200px;margin-top: 10px;">Посмотреть портфолио</button>
							</a>
						<div>
					@endif
                </div>
            </div>
        </div>
    </div>
@endsection