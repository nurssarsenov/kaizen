@extends('web.master')
@section('content')
    <div class="section_container">
        <div class="ui container">
            <div class="section_container__white">
                <p class="section_container__title">Регистрация</p>
                <div class="section_container__tab">
                    <ul>
                        <li class="active"><a href="{{route('courseRegistration',$course['id'])}}">Я участник</a></li>
                        <li><a href="{{route('courseRegistrationMembers',$course['id'])}}">Добавить участника</a>
                        </li>
                    </ul>
                </div>
                <div class="section_container__tab___content">
                    <div class="ui form">
                        <form action="#" id="registerForm">
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Ф.И.О</label>
                                    @if(auth()->check())
                                    <h4> {{auth()->user()->fio}} </h4>
                                        <input type="hidden" value="{{auth()->user()->fio}}" name="fio" id="fio" placeholder="Фамилия Имя Отчество">
                                    @else
                                    <input type="text" name="fio" id="fio" placeholder="Фамилия Имя Отчество">
                                    @endif
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Должность</label>
                                    @if(auth()->check())
                                        <h4> {{auth()->user()->position}} </h4>
                                    <input type="hidden" value="{{auth()->user()->position}}" name="position" id="position" placeholder="Должность">
                                    @else
                                        <input type="text" name="position" id="position" placeholder="Должность">@endif
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Компания</label>
                                    @if(auth()->check())
                                        <h4> {{auth()->user()->company}} </h4>
                                    <input type="hidden" value="{{auth()->user()->company}}" name="company" id="company" placeholder="Компания">
                                    @else
                                        <input type="text" name="company" id="company" placeholder="Компания">@endif
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Город</label>
                                    @if(auth()->check())
                                        <h4> {{auth()->user()->city}} </h4>
                                    <input type="hidden" value="{{auth()->user()->city}}" name="city" id="city" placeholder="Город">
                                    @else
                                        <input type="text" name="city" id="city" placeholder="Город">@endif
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>E-mail</label>
                                    @if(auth()->check())
                                        <h4> {{auth()->user()->email}} </h4>
                                    <input type="hidden" value="{{auth()->user()->email}}" name="email" id="email" placeholder="E-mail">
                                    @else
                                        <input type="text" name="email" id="email" placeholder="E-mail">@endif
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Телефон</label>
                                    @if(auth()->check())
                                        <h4> {{auth()->user()->phone}} </h4>
                                    <input type="hidden" value="{{auth()->user()->phone}}" name="phone" id="phone" data-js="input" placeholder="Телефон" autocomplete="tel-local">
                                    @else
                                        <input type="text" name="phone" id="phone" data-js="input" placeholder="Телефон" autocomplete="tel-local">@endif
                                </div>
                            </div>
                            <input type="hidden" name="course" id="course" data-js="input" value="{{$course['id']}}">
                            <div class="section_container__bottom">
                                <p><span>*</span> Нажимая кнопку «Зарегистрироваться», Вы даете группе компаний Kaizen
                                    <a href="#">согласие на обработку </a>своих персональных данных</p>
                            </div>
                            <div class="section_container__button">
                                <button class="ui orange submit " type="button" onclick="Register.courseRegister();">
                                    ПРОДОЛЖИТЬ
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection