<div class="ui doubling stackable  grid">
    <div class="four wide  column">
        <p class="list__members">Участников:</p>
        <p class="list__price">Стоимость:</p>
        <p class="list__all_price">Всего:</p>
    </div>
    <div class="eleven wide  column">
        <p class="list__members">{{$payment['total_users']}}</p>
        <p class="list__price">{{number_format($course['price'], 0,",",".")}}
            KZT</p>
        <p class="list__all_price">{{number_format($payment['payment_amount'], 0,",",".")}}
            KZT</p>
    </div>
</div>