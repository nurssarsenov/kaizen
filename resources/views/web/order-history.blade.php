@extends('web.master')
<style>.pagination {
        list-style: none;
        padding-left: 0;
        text-align: center;
    }

    .pagination li {
        display: inline-block;
    }

    .pagination li+li {
        margin-left: 1rem;
    }

    .pagination a {
        text-decoration: none;
        padding: 0.2rem 0.4rem;
        color: red;
        border: 1px solid red;
        border-radius: 2px;
    }</style>
@section('content')
    <div class="section_container">
        <div class="container">
            <div class="section_container__white">
                <p class="section_container__title">Регистрация</p>
                        <table class="table table-bordered" border="2px">
                            <thead>
                            <tr>
                                <th scope="col">Ф.И.О.</th>
                                <th scope="col">Еmail</th>
                                <th scope="col">Наименование круса</th>
                                <th scope="col">Цена курса</th>
                                <th scope="col">Статус</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($registrationUsers as $registrationUser)
                                <tr>
                                    <td style="padding-right: 5%">{{$registrationUser->fio}}</td>
                                    <td style="padding-right: 5%">{{$registrationUser->email}}</td>
                                    <td style="padding-right: 5%">{{$registrationUser->course_title}}</td>
                                    <td style="padding-right: 5%">{{$registrationUser->price}}</td>
                                    <td style="padding-right: 5%">{{$registrationUser->is_paid? "Оплачено":"В ожидании"}}
                                        <br>
                                    @if(!$registrationUser->is_paid) <a href="/payments?token={{$registrationUser->token}}">Оплатить</a> @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

            </div>
            {{ $registrationUsers->links() }}
        </div>
    </div>
@endsection
