<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Кайдзен Центр</title>
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/web/assets/libs/semantic/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/web/assets/libs/fancybox/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/web/assets/dist/assets/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="/assets/web/assets/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/web/assets/libs/toastr/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/web/assets/css/main.css?11082018">
</head>
<body>
<header>
    <div class="header_section">
        <div class="ui container grid">
            <div class="two wide column">
                <div class="header_logo">
                    <a href="/">
                        <img src="/assets/web/img/logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="twelve wide column">
                <ul class="header_menu menu_navigator">
                    {!! $menu_header->content !!}
                </ul>
            </div>
            <div class="two wide column">
                <div class="top-right links">
                    @auth
                        <div class="dropdown text-right">

                            <button class="btn btn-outline-dark font-weight-bold dropdown-toggle" type="button"
                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false"
                                    style="border-radius: 18px; font-size: 15px; margin-top: 17px;">
                                Личный кабинет
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ url('/history') }}">
                                    Посмотреть историю заказов
                                </a>
                                <a class="dropdown-item" href="/auth/logout">
                                    Выход
                                </a>
                            </div>
                        </div>
                    @else
                        <div class="text-right">
                            <a class="btn btn-outline-dark font-weight-bold" href="{{ url('/userLogin') }}"
                               style="border-radius: 18px; font-size: 15px; margin-top: 17px;">
                                ВХОД
                            </a>
                        </div>
                    @endauth
                </div>
            </div>
        </div>
    </div>
    <div class="mobile">
        <div class="ui container grid">
            <div class="seven wide column">
                <div class="header_logo">
                    <a href="/">
                        <img src="/assets/web/img/logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="nine wide column">
                <a href="#" class="burger-icon toggle-navbar burger-left" data-toggle="0">
                    <span></span><span></span><span></span>
                </a>
            </div>
        </div>
    </div>
    <div class="mobile_header  menu_hide">
        <div class="ui container">
            <ul class="mobile_header__menu menu_navigator">
                {!! $menu_header->content !!}
                <div class="two wide column">
                    <div class="top-right links">
                        @auth
                            <a class="" href="{{ url('/history') }}">Посмотреть историю заказов</a>
                            <div class="right menu">
                                <a class="ui item" href="/auth/logout">
                                    Выход
                                </a>
                            </div>
                        @else
                            <a class="" href="{{ url('/userLogin') }}"
                               style="border-radius: 18px; font-size: 16    px; margin-top: 17px;">
                                ВХОД
                            </a>
                        @endauth
                    </div>
                </div>
            </ul>

        </div>
    </div>
</header>
@yield('content')

<footer>
    <div class="footer_section">
        <div class="ui container">
            <div class="ui doubling stackable grid">
                <div class="two wide column">
                    <div class="footer_logo">
                        <a href="">
                            <img src="/assets/web/img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="ten wide column">
                    <ul class="footer_menu" id="menuBottom">
                        {!! $menu_footer->content !!}
                    </ul>
                </div>
                <div class="four wide column">
                    <ul class="footer_social">
                        {!! $social->content !!}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="/assets/web/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/web/assets/js/jquery.inputmask.js"></script>
<script type="text/javascript" src="/assets/web/assets/libs/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="/assets/web/assets/libs/semantic/semantic.min.js"></script>
<script type="text/javascript" src="/assets/web/assets/dist/owl.carousel.min.js"></script>
<script type="text/javascript" src="/assets/web/assets/libs/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/assets/web/assets/js/register.js?20092018"></script>
<script type="text/javascript" src="/assets/admin/js/auth/auth.js"></script>
<script type="text/javascript" src="/assets/web/assets/js/main.js?20092018"></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
@if(request()->path() == '/')
    <script type="text/javascript">
        $(".menu_navigator").on("click", "a", function (event) {
            event.preventDefault();
            var id = $(this).attr('href');
            var top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1500);
        });
        $("#menuBottom").on("click", "a", function (event) {
            event.preventDefault();
            var id = $(this).attr('href'),
                top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1500);
        });
    </script>
@else
    <script type="text/javascript">
        $(".menu_navigator").on("click", "a", function (event) {
            event.preventDefault();
            var id = $(this).attr('href');
            window.location.href = '/' + id;
        });
        $("#menuBottom").on("click", "a", function (event) {
            event.preventDefault();
            var id = $(this).attr('href');
            window.location.href = '/' + id;
        });
    </script>
@endif
@stack('scripts')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126481346-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-126481346-1');

    $(document).ready(function () {
        $(".dropdown-toggle").dropdown();
    });
</script>

</body>
</html>
