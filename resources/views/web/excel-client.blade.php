<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Excel</title>
</head>
<body>
<table>
    <tr style="border: 1px solid #000;">
        <th>ID</th>
        <th>Email</th>
        
    </tr>
    @if(isset($data))
        @foreach($data as $item)
        <tr>
            <td>{{$item['id']}}</td>
           
            <td>{{$item['email']}}</td>
        </tr>
        @endforeach
    @endif
</table>
</body>
</html>