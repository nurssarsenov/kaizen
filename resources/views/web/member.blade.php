<div class="content_members">
    <div class="ui fluid form">
        <div class="ui accordion field">
            <div class="members_list_content title">
                <div class="members_list">
                    <p class="list_title added">{{$user['fio']}}</p>
                </div>
            </div>
            <div class="content field members_list_content">
                <form action="#" class="memberUpdate">
                    <div class="members_list_form">
                        <div class="ui form">
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Ф.И.О</label>
                                    <input type="text" name="fio" class="fio"
                                           placeholder="Фамилия Имя Отчество"
                                           value="{{$user['fio']}}">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Должность</label>
                                    <input type="text" name="position" class="position"
                                           placeholder="Должность"
                                           value="{{$user['position']}}">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Компания</label>
                                    <input type="text" name="company" class="company"
                                           placeholder="Компания"
                                           value="{{$user['company']}}">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Город</label>
                                    <input type="text" name="city" class="city"
                                           placeholder="Город"
                                           value="{{$user['city']}}">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>E-mail</label>
                                    <input type="text" name="email" class="email"
                                           placeholder="E-mail"
                                           value="{{$user['email']}}">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Телефон</label>
                                    <input type="text" name="phone" class="phone"
                                           data-js="input"
                                           placeholder="Телефон"
                                           value="{{$user['phone']}}">
                                </div>
                            </div>
                            <input type="hidden" name="userId" class="userId"
                                   data-js="input" value="{{$user['id']}}">
                            <input type="hidden" name="token" class="token"
                                   data-js="input"
                                   value="{{isset(request()->token) ? request()->token : ''}}">
                            <div class="section_container__bottom">
                                <a href="javascript:void(0);"
                                   class="ui added_link  submit"
                                   onclick="Register.memberUpdate($(this))">Сохранить</a>
                                <a href="javascript:void(0);" class="ui remove_link"
                                   onclick="Register.memberRemove($(this))">Удалить</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>