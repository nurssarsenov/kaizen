@extends('web.master')
@section('content')
    <div class="section_container">
        <div class="ui container">
            <div class="section_container__white">
                <div class="section_container__white___step">
                    <ul class="progressbar">
                        <li class="load active">Регистрация</li>
                        <li class="load active">Подтверждение</li>
                        <li class="load ">Способы оплаты</li>
                        <li class="load ">Оплата</li>
                    </ul>
                </div>
                @if(isset($data))
                    <div class="section_container__tab___content">
                        <p class="section_end_registration">Регистрация пройдена</p>
                        <p class="section_end_registration__description">Вы успешно зарегистрированы на курс
                            "{{$data['courses']['title']}}"</p>
                        <p class="section_end_registration___date">Регистрация участников
                            на {{date('d-m-Y',strtotime($data['courses']['start_date']))}} </p>
                        @if(count( $data['users']))
                            <div class="section_registration__list">
                                <ul>
                                    @foreach( $data['users'] as $i=> $user)
                                        <li id="{{$user['id']}}">{{$i+1}}.{{$user['fio']}} <span><a class="delete-user"
                                                                                                    data-payment="{{$data['courses']['payment_id']}}"
                                                                                                    href="javascript:void(0);"
                                                                                                    data-id="{{$user->id}}">УБРАТЬ</a></span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <p class="section_registration__list__add"
                           onclick="window.location.href='/registration-members/{{$data['courses']['id']}}?token={{request()->token}}'">
                            Добавить еще</p>
                        <div class="section_registration__list__all">
                            <div class="ui doubling stackable  grid">
                                <div class="four wide  column">
                                    <p class="list__members">Участников:</p>
                                    <p class="list__price">Стоимость:</p>
                                    <p class="list__all_price">Всего:</p>
                                </div>
                                <div class="eleven wide  column">
                                    <p class="list__members">{{$data['courses']['total_users']}}</p>
                                    <p class="list__price">{{number_format($data['courses']['price'], 0,",",".")}}
                                        KZT</p>
                                    <p class="list__all_price">{{number_format($data['courses']['payment_amount'], 0,",",".")}}
                                        KZT</p>
                                </div>
                            </div>
                        </div>
                        <form action="/pay" method="POST" id="pay">
                        <div class="section_registration__list__pay">
                            <ul>
								<!--<li><p style="color:red">Онлайн оплата временно недоступна. </p></li>-->
                                <li>
                                    <div class="field">
                                        <div class="ui radio checkbox">
                                            <input type="radio" name="pay" checked="" class="hidden" value="1">
                                            <label>Оплата картой</label>
                                        </div>
                                        <img class="pay_logo" src="/assets/web/img/Radiobuttoninactiv.png" alt="">
                                    </div>
                                </li>
                                <li>
                                    <div class="field">
                                        <div class="ui radio checkbox">
                                            <input type="radio" name="pay" class="hidden" value="0">
                                            <label>Оплатить по счету (для юр.лиц и ИП)</label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                            <input type="hidden" name="token" value="{{request()->token}}">
                            {{csrf_field()}}
                            <p class="repay"><a href="javascript:void(0);" onclick="$('#pay').submit()">Оплатить</a></p>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection