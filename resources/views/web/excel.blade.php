<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Excel</title>
</head>
<body>
<table>
    <tr style="border: 1px solid #000;">
        <th>ID</th>
        <th>ФИО</th>
        <th>Email</th>
        <th>Номер телефона</th>
        <th>Город</th>
        <th>Компания</th>
        <th>Должность</th>
        <th>Курс</th>
        <th>Цена</th>
        <th>Дата заявки</th>        <th>Статус</th>
        <th>дата начала</th>
    </tr>
    @if(isset($data))
        @foreach($data as $item)
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->fio}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->phone}}</td>
            <td>{{$item->city}}</td>
            <td>{{$item->company}}</td>
            <td>{{$item->position}}</td>
            <td>{{$item->title}}</td>
            <td>{{$item->price}}</td>
            <td>{{$item->created_at}}</td>            <td>				@if($item->is_paid == 1)					Оплачено				@else					Не оплачено				@endif			</td>
            <td>{{$item->start_date}}</td>
        </tr>
        @endforeach
    @endif
</table>
</body>
</html>