@extends('web.master')
@section('content')
    @if(isset($data['section_wrapper']))
        <div class="section_wrapper"
             style="background-image: url({{isset($data['section_wrapper']['file']) ? asset('uploads/'.$data['section_wrapper']['file']) :  asset('assets/web/img/qwert.png')}});">
            <div class="ui container">
                <h1 class="section_wrapper__title">{{$data['section_wrapper']['title']}}</h1>
                <p class="section_wrapper__text">{{$data['section_wrapper']['sh_content']}}</p>
                <div class="text-center mt-4">
                    <a type="button" class="btn btn-lg" data-fancybox
                            href="https://www.youtube.com/watch?v=KWp1oP9DJso" style="background-color: #ff521e;color: #ffffff; border-radius: 18px;">
                        ПОДРОБНЕЕ
                    </a>
                </div>
            </div>
        </div>
    @endif
    @if(isset( $data['section_file']))
        <div class="section_file" id="section_file">
            <div class="ui container">
                <p class="section_file__title">{{ $data['section_file']['title']}}</p>
                <p class="section_file__text">{{$data['section_file']['sh_content']}}</p>
                <div class="section_file__download">
                    <div class="ui doubling stackable  grid">
                        <div class="eleven wide column">
                            <p class="section_file__download___title">{{$data['section_file']['content']}}</p>
                        </div>
                        <div class="five wide column">
                            <button class="section_file__download___btn" data-file="{{$data['section_file']['file']}}">
                                СКАЧАТЬ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(isset($data['section_information']))
        <div class="section_information" id="section_information">
            <div class="ui container">
                <h1 class="section_information__title">{{$data['section_information']['title']}}</h1>
                <p class="section_information__text">{{$data['section_information']['sh_content']}}</p>
                <div class="section_information__content">
                    <div class="ui stackable two column grid">
                        @if(count($data['section_information']->contents))
                            @foreach($data['section_information']->contents as $content)
                                <div class="column">
                                    <div class="section_information__content___column">
                                        <img class="section_information__content___column____img"
                                             src="/uploads/{{$content['img']}}" alt="">
                                        <p class="section_information__content___column____title">{{$content['title']}}</p>
                                        <p class="section_information__content___column____content">{{$content['sh_content']}}</p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(isset($data['section_start']))
        <div class="section_start" id="section_start"
             style="background-image: url({{isset($data['section_start']['file']) ? asset('uploads/'.$data['section_start']['file']) : asset('assets/web/img/demo-header.png')}});">
            <div class="ui container">
                <p class="section_start__title">
                    {{$data['section_start']['title']}}
                </p>
                @if(count($data['section_start']->contents))
                    <div class="ui doubling stackable   three column centered grid">
                        <?php $contents = $data['section_start']->contents; ?>
                        @foreach($contents as $i => $item)
                            <div class="column">
                                <div class="section_start__column">
                                    <p class="section_start__column___number">
                                        . {{$i+1}}
                                    </p>
                                    <p class="section_start__column___content">
                                        {{$item['title']}}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    @endif
    @if(isset($data['section_courses']))
        <div class="section_courses" id="section_courses">
            <div class="ui container">
                <p class="section_courses__title">
                    {{$data['section_courses']['title']}}
                </p>
                <p class="section_courses__text">
                    {{$data['section_courses']['sh_content']}}
                </p>
                <div class="section_courses__content">
                    <div class="ui doubling  stackable three column grid">
                        @if(isset($data['section_courses_items']))
                            @foreach($data['section_courses_items'] as $course)
                                <div class="column">
                                    <div class="section_courses__content___column">
                                        <div class="section_courses__content___column____days">
                                            <p class="column_day">{{$course['start_date']}}</p>
                                            <p class="column_day__week">{{$course['week']}}</p>
                                        </div>
                                        <div class="section_courses__content___column____title">
                                            <p class="column_title">{{$course['title']}}</p>
                                        </div>
                                        <div class="section_courses__content___cols">
                                            <p class="section_courses__content___column____term">{{$course['sh_content']}}</p>
                                            <p class="section_courses__content___column____desc">{{$course['content']}}</p>
{{--                                            <a target="_blank" href="{{ $course['link'] }}"><p class="section_courses__content___column____desc" style="color: #0d71bb">{{ $course['link'] }}</p></a>--}}
                                        </div>
                                        @if(isset($course['file']))
                                            <div class="section_courses__content___column____file">
                                                <a href="uploads/{{$course['file']}}"
                                                   download="uploads/{{$course['file']}}">Скачать файл</a>
                                            </div>
                                        @endif
                                        <div class="section_courses__content___column____price">
                                            <p class="column_price">{{number_format($course['price'], 0,",",".")}}
                                                тенге</p>
                                            <p class="column_price__title">{{isset($course['price_content']) ? '('.$course['price_content'].')' : ''}}</p>
                                        </div>
                                        @if(isset($course['link']))
                                            <a target="_blank" href="{{ $course['link'] }}">
                                            <button class="section_courses__content___column____btn">
                                                ПЕРЕЙТИ НА САЙТ
                                            </button>
                                            </a>
                                        @else
                                            <button class="section_courses__content___column____btn"
                                                    onclick="window.location.href='/registration/{{$course['id']}}'">
                                                ПОДАТЬ ЗАЯВКУ
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(isset($data['section_file_2']))
        <div class="section_file section_file_2" id="section_file_2">
            <div class="ui container">
                <p class="section_file__title">{{$data['section_file_2']['title']}}</p>
                <p class="section_file__text">{{$data['section_file_2']['sh_content']}}</p>
                <div class="section_file__download_2">
                    <button class="section_file__download___btn_2" data-file="{{$data['section_file_2']['file']}}">
                        СКАЧАТЬ
                    </button>
                </div>
                <p class="section_file__text">{{$data['section_file_2']['content']}}</p>
            </div>
        </div>
    @endif
    @if(isset($data['section_commands']))
        <div class="section_commands" id="section_commands">
            <div class="ui container">
                <p class="section_commands__title">{{$data['section_commands']['title']}}</p>
                <div class="section_commands__command">
                    <div class="ui doubling stackable three column grid">
                        @if(count($data['section_commands']->contents))
                            @foreach($data['section_commands']->contents as $command)
                                @if($command['is_active'] == 1)
                                    <div class="column">
                                        <div class="section_commands__command___column">
                                            <div class="command_column__img">
                                                <img src="/uploads/{{$command['img']}}" alt="">
                                            </div>
                                            <div class="command_column__content">
                                                <p class="command_column__content___title">{{$command['title']}}</p>
                                                <p class="command_column__content___text">{{$command['sh_content']}}</p>
                                                <div style="text-align: center;">
                                                    <button class="section_courses__content___column____btn"
                                                            onclick="window.location.href='/team/{{$command['id']}}'"
                                                            style="max-width: 200px;margin-top: 10px;">Подробнее
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(isset($data['section_gallery']))
        <div class="section_gallery" id="section_gallery">
            <div class="ui container">
                <p class="section_gallery__title">{{$data['section_gallery']['title']}}</p>
                <div class="section_gallery__lists">
                    <div class="ui grid doubling stackable four column">
                        @if(count($data['section_gallery']->contents))
                            @foreach($data['section_gallery']->contents as $gallery)
                                <div class="column">
                                    <div class="section_gallery__lists___img">
                                        <a data-fancybox="gallery" href="/uploads/{{$gallery->img}}"><img
                                                    src="/uploads/{{$gallery->img}}"
                                                    alt=""></a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif
    {{--@if(isset($data['section_reviews']))
        <div class="section_reviews" id="section_reviews">
            <div class="ui container">
                <p class="section_reviews__title">{{$data['section_reviews']['title']}}</p>
                <div class="owl-carousel">
                    @if($data['section_reviews']->contents)
                        @foreach($data['section_reviews']->contents as $reviews)
                            <div class="ui doubling stackable  grid ">
                                <div class="three wide column">
                                    <div class="section_reviews_img">
                                        <img src="/uploads/{{$reviews['img']}}" alt="">
                                    </div>
                                </div>
                                <div class="thirteen wide column">
                                    <p class="section_reviews__text">{{$reviews['content']}}</p>
                                    <p class="section_reviews__author">{{$reviews['title']}}</p>
                                    <p class="section_reviews__position">{{$reviews['sh_content']}}</p>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    @endif--}}
    <div class="section_contacts" id="section_contacts">
        <div class="ui container">
            <div class="ui grid stackable two column">
                <div class="column">
                    <div class="section_contacts__left">
                        <ul class="section_contacts__left___top">
                            @if(isset($data['requisites']))
                                {!! $data['requisites']['content'] !!}
                            @endif
                        </ul>
                        <ul class="section_contacts__left___address">
                            @if(isset($data['address']))
                                {!! $data['address']['content'] !!}
                            @endif
                            @if(isset($data['emails']))
                                {!! $data['emails']['content'] !!}
                            @endif
                        </ul>
                        <ul class="section_contacts__left___phones">
                            @if(isset( $data['phones']))
                                {!!  $data['phones']['content'] !!}
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="column">
                    @if(isset( $data['map']))
                        {!!  $data['map']['content'] !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <form id="sendEmail">
        <div class="ui mini modal modal-download-pdf">
            <div class="header">Введите электронный адрес</div>
            <div class="scrolling content">
                <div class="ui fluid  input">
                    <input type="text" name="email" class="email" placeholder="пример@mail.ru">
                </div>
            </div>
            <div class="actions">
                <button class="ui button" type="button" onclick="Send.sendEmail($(this));">Скачать</button>
                <button class="ui cancel button">Отмена</button>
            </div>
        </div>
    </form>
@endsection