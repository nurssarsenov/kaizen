@extends('web.master')
@section('content')
    <div class="section_container">
        <div class="ui container">
            <div class="section_container__white">
                <p class="section_container__title">Счёт на оплату</p>
                <div class="section_container__tab___content">
                    <div class="ui form">
                        <form action="#" id="entityForm">
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>БИН / ИИН</label>
                                    <input type="text" name="bin_iin" id="bin_iin" placeholder="БИН / ИИН">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Компания</label>
                                    <input type="text" name="company" id="company" placeholder="компания">
                                </div>
                            </div>
                            <div class="inline fields">
                                <div class="sixteen wide field">
                                    <label>Адрес</label>
                                    <input type="text" name="address" id="address" placeholder="адрес">
                                </div>
                            </div>
                            <input type="hidden" name="token" id="token" value="{{$_REQUEST['token']}}">
                            <div class="section_container__button">
                                <button class="ui orange submit " type="button" onclick="Register.downloadPdf();">
                                    Скачать счёт в формате pdf
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection