<table>
    <tbody>
    <tr>
        <td colspan="14"></td>
        <td colspan="4" align="center">
            <i>Приложение 50</i>
        </td>
    </tr>
    <tr>
        <td colspan="14"></td>
        <td colspan="4" align="center">
            <i>к приказу Министра финансов</i>
        </td>
    </tr>
    <tr>
        <td colspan="14"></td>
        <td colspan="4" align="center"><i>Республики Казахстан</i></td>
    </tr>
    <tr>
        <td colspan="14"></td>
        <td colspan="4" align="center"><i>от 20 декабря 2012 года № 562</i></td>
    </tr>
    <tr>
        <td colspan="20"></td>
    </tr>
    <tr>
        <td colspan="15"></td>
        <td colspan="3" align="right">Форма Р-1</td>
    </tr>
    <tr>
        <td colspan="20"></td>
    </tr>
    <tr>
        <td colspan="2" style="vertical-align: middle">Заказчик</td>
        <td colspan="12" align="center" style="border-bottom: 1px solid #000; vertical-align: middle"><b>Товарищество с ограниченной
                ответственностью "Pointex.KZ",Республика Казахстан,
                г. Алматы, ул. Розыбакиева, дом № 37В</b>
        </td>
        <td colspan="2"></td>
        <td colspan="2" style="vertical-align: middle; border: 1px solid #000; text-align: center; font-weight: bold">130740014863</td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td colspan="14" align="center"><i>полное наименование, адрес, данные о средствах связи</i>
        </td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2" style="vertical-align: middle">Исполнитель</td>
        <td colspan="12" align="center" style="border-bottom: 1px solid #000; vertical-align: middle"><b>ТОО "Центр производительности и качества",050060, Республика Казахстан, г. Алматы, ул. Розыбакиева, дом № 227</b>
        </td>
        <td colspan="2"></td>
        <td colspan="2" style="vertical-align: middle; border: 1px solid #000; text-align: center; font-weight: bold">170640007369</td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td colspan="14" align="center"><i>полное наименование, адрес, данные о средствах связи</i>
        </td>
        <td colspan="2"></td>
    </tr>
    </tbody>
</table>