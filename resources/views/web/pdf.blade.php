<?php
function num2str($num)
{
    $nul = 'ноль';
    $ten = array(
        array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
    );
    $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
    $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
    $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
    $unit = array( // Units
        array('тиын', 'тиын', 'тиын', 1),
        array('тенге', 'тенге', 'тенге', 0),
        array('тысяча', 'тысячи', 'тысяч', 1),
        array('миллион', 'миллиона', 'миллионов', 0),
        array('миллиард', 'милиарда', 'миллиардов', 0),
    );
    //
    list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub) > 0) {
        foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
            if (!intval($v)) continue;
            $uk = sizeof($unit) - $uk - 1; // unit key
            $gender = $unit[$uk][3];
            list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
            // mega-logic
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
            else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            // units without rub & kop
            if ($uk > 1) $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
        } //foreach
    } else $out[] = $nul;
    $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
    $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
}

/**
 * Склоняем словоформу
 * @ author runcore
 */
function morph($n, $f1, $f2, $f5)
{
    $n = abs(intval($n)) % 100;
    if ($n > 10 && $n < 20) return $f5;
    $n = $n % 10;
    if ($n > 1 && $n < 5) return $f2;
    if ($n == 1) return $f1;
    return $f5;
}
?>
        <!DOCTYPE html>

<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    {{--    {{dd($data,$payments)}}--}}
    <title>Счет №{{$data['id']}}</title>
    

    <style type="text/css">
        body {
            font-family: DejaVu Sans;
        }

        .top {
            font-size: 10px;
            max-width: 500px;
            text-align: center;
            margin: 0 auto;
        }

        table {
            border-collapse: collapse;
            font-size: 12px;
        }

        .one td, .three td {
            border-color: #000;
            border-style: solid;
            border-width: 1px;
        }

        .width {
            width: 330px;
            padding: 0 0 0 10px;
        }

        .center {
            text-align: center;
            padding: 0 10px;
        }

        .bold {
            font-weight: bold;
        }

        .two tr td {
            padding: 15px 0 0 0;
        }

        .n1 {
            width: 30px;
            text-align: center;
        }

        .n2 {
            width: 100px;
            text-align: center;
        }

        .n3 {
            width: 255px;
            padding: 0 10px;
        }

        .n4 {
            width: 50px;
            text-align: center;
        }

        .n5 {
            width: 50px;
            text-align: center;
        }

        .n6, .n7 {
            width: 100px;
            text-align: center;
        }

        .three {
            margin: 50px 0 0 0;
        }

        .table_bottom {
            font-weight: bold;
            font-size: 13px;
            float: right;
        }

        .name {
            margin-top: 30px;
            font-size: 13px;
        }

        .name_bold {
            margin-top: -17px;
            font-size: 13px;
        }

        .pl {
            font-size: 13px;
            position: relative;
        }

    </style>

</head>

<body>
<p class="top">" Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате
    обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпускается по факту прихода денег
    на р/с Поставщика, самовывозом, при наличии доверенности и документов удостоверяющих личность."
</p>
<p style="margin-top: 50px;">Платежное поручение</p>
<table class="one">
    <tr>
        <td class="width"><span class="bold">Бенефициар: <br>ТОО "Центр производительности и качества" </span><br>БИН:
            170640007369
        </td>
        <td class="center">ИИК <br> KZ319470398922617013</td>
        <td class="center">Кбе <br>17</td>
    </tr>
    <tr>
        <td class="width">Банк бенефициара: <br>АО ДБ «Альфа-Банк»</td>
        <td class="center">БИК <br>ALFAKZKA</td>
        <td class="center">Код назначения платежа <br>861</td>
    </tr>
</table>
<?php
$arr = ['01' => 'января', '02' => 'февраля', '03' => 'марта', '04' => 'апреля', '05' => 'мая', '06' => 'июня', '07' => 'июля', '08' => 'августа', '09' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря'];
$explode = explode('-', $data['date']);
$date_string = $explode[2] . ' ' . $arr[$explode[1]] . ' ' . $explode[0];
?>
<p>Счет на оплату № {{$data['id']}} от {{$date_string}} г.</p>
<hr>
<table class="two">
    <tr>
        <td>Поставщик:</td>
        <td> БИН / ИИН 170640007369,ТОО "Центр производительности и качества",050060, Республика Казахстан, г. Алматы,
            ул. Шумный 2
        </td>
    </tr>
    <tr>
        <td>Покупатель:</td>
        <td> БИН / ИИН {{$data['bin_iin']}}, {{$data['company']}}, {{$data['address']}}
        </td>
    </tr>
    <tr>
        <td>Договор:</td>
        <td>Без договора</td>
    </tr>
</table>
@if(isset($payment))
    <table class="three">
        <tr>
            <td class="n1">№</td>
            <td class="n2">Код</td>
            <td class="n3">Наименование</td>
            <td class="n4">Кол-во</td>
            <td class="n5">Ед.</td>
            <td class="n6">Цена</td>
            <td class="n7">Сумма</td>
        </tr>
        <tr>
            <td class="n1">1</td>
            <td class="n2">{{str_pad($payment->id, 11, "0", STR_PAD_LEFT)}}</td>
            <td class="n3">{{$payment->title}}</td>
            <td class="n4">{{$payment->total_users}}</td>
            <td class="n5">усл.</td>
            <td class="n6">{{$payment->price}}, 00</td>
            <td class="n7">{{$payment->payment_amount}}, 00</td>
        </tr>
    </table>
    <p class="table_bottom">Итого: {{$payment->payment_amount}}, 00</p>

    <p class="name">Всего наименований 1, на сумму {{$payment->payment_amount}},00 KZT</p>
    <p class="name_bold"><b>Всего к оплате: {{num2str($payment->payment_amount)}}</b></p>
    <hr>
    <br>
    <p class="pl">Исполнитель: &nbsp;&nbsp;&nbsp;<img style="position: absolute; top: -15px;" src="{{asset('public/assets/web/img/potpis.jpg')}}" alt=""></p>
    <img  src="{{asset('public/assets/web/img/unnamed1.jpg')}}" alt="">
@endif
</body>

</html>
