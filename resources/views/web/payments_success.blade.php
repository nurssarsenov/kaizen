@extends('web.master')
@section('content')
  <div class="section_container">
    <div class="ui container">
      <div class="section_container__white">
        <div class="section_container__white___step">
          <ul class="progressbar">
            <li class="load active">Регистрация</li>
            <li class="load active">Подтверждение</li>
            <li class="load active">Способы оплаты</li>
            <li class="load active">Оплата</li>
          </ul>
        </div>
        <div class="section_container__tab___content">
          @if(\Illuminate\Support\Facades\Request::has('token'))
          <h3>Регистрация успешно завершена. Проверьте свою электронную почту.</h3>
          @else
          <h3>Оплата успешно завершена. Проверьте свою электронную почту.</h3>
          @endif
          <a href="/#section_courses" class="section_end_registration" style="float: right">назад</a>
        </div>
      </div>
    </div>
  </div>
@endsection