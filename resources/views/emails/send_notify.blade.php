
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Notification2</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!--<![endif]-->

    <style type="text/css">

        .ReadMsgBody {
            width: 100%;
            background-color: #D2D7D3;
        }

        .ExternalClass {
            width: 100%;
            background-color: #D2D7D3;
        }

        .a {
            text-decoration: none;
        }

        body {
            width: 100%;
            background-color: #d8eaf8;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            font-family: OpenSans, Arial, Helvetica Neue, Helvetica, sans-serif
        }

        @-ms-viewport {
            width: device-width;
        }

        @import url('https://fonts.googleapis.com/css?family=Roboto');
        * {
            font-family: 'Roboto', sans-serif !important;
        }

        @media only screen and (max-width: 639px) {
            .wrapper {
                width: 100%;
                padding: 0 !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .centerClass {
                margin: 0 auto !important;
            }

            .imgClass {
                width: 100% !important;
                height: auto;
            }

            .img {
                text-align: center
            }

            .hide {
                display: none;
            }

            .wrapper {
                width: 100%;
                padding: 0 !important;
            }

            .header {
                width: 100%;
                padding: 0 !important;
                background-image: url(http://placehold.it/320x400) !important;
            }

            .container {
                width: 300px;
                padding: 0 !important;
            }

            .box {
                width: 250px;
                padding: 0 !important;
            }

            .mobile {
                width: 300px;
                display: block;
                padding: 0 !important;
                text-align: center !important;
            }

            .mobile50 {
                width: 300px;
                padding: 0 !important;
                text-align: center;
            }

            *[class="mobileOff"] {
                width: 0px !important;
                display: none !important;
            }

            *[class*="mobileOn"] {
                display: block !important;
                max-height: none !important;
            }
        }

        .MsoNormal {
            font-family: Montserrat, OpenSans, Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }
    </style>

    <!--[if gte mso 15]>
    <style type="text/css">
        table {
            font-size: 1px;
            line-height: 0;
            mso-margin-top-alt: 1px;
            mso-line-height-rule: exactly;
        }

        * {
            mso-line-height-rule: exactly;
        }
    </style>
    <![endif]-->

</head>
<body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0"
      style="background-color:#D2D7D3;  font-family: Montserrat, OpenSans, Arial, sans-serif; color:#ffffff; text-decoration:none; margin:0; padding:0; min-width: 100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">

<!-- Start Background -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#D2D7D3">
    <tr>
        <td width="100%" valign="top" align="center">


            <table>
                <tbody>
                <tr>
                    <td height="50"
                        style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                </tr>
                <tr>
                    <td align="center">

                        <!-- Start Container -->
                        <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff"
                               class="container">
                            <tbody>
                            <tr>
                                <td align="center" bgcolor="#E35E31"
                                    style="font-size: 35px; line-height: 30px; color: #ffffff; font-weight: 700;"></td>
                            </tr>
                            <tr>
                                <td height="50"></td>
                            </tr>
                            <tr>
                                <td align="center" class="mobile" style="font-size:12px; line-height:24px;">
                                    <table width="400" cellpadding="0" cellspacing="0" border="0" class="container">
                                        <tbody>
                                        <tr>
                                            <td width="350" align="center" class="mobile"
                                                style="font-size:12px; line-height:24px;">
                                                <!-- Start Content -->
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                       class="container">
                                                    <tbody>
                                                    <tr>
                                                        <td width="330" align="center"
                                                            style="font-family: Montserrat, Arial, sans-serif; font-size: 28px; line-height:40px; letter-spacing:2px; color: #333333; font-weight:400;"
                                                            data-color="H1 Color" data-size="H1 Size" data-min="12"
                                                            data-max="40" mc:edit="">
                                                            <multiline label="content">Здравствуйте!</multiline>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20" style="font-size:10px; line-height:10px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20" align="center"
                                                            style="font-family: Montserrat, Arial, sans-serif; font-size:16px; letter-spacing:1px; line-height:25px; color: #333333; font-weight:400"
                                                            data-color="H4 Color" data-size="H4 Size" data-min="12"
                                                            data-max="30" mc:edit="">
                                                            <multiline label='content'>{{$user['fio']}}</multiline>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20" style="font-size:10px; line-height:10px;"></td>
                                                        <!-- Spacer -->
                                                    </tr>
                                                    <tr>
                                                        <td align="center"
                                                            style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #8D8D8D; line-height:23px; font-weight:400;"
                                                            data-color="H6 Color" data-size="H6 Size" data-min="10"
                                                            data-max="24" mc:edit="">
                                                            <multiline label="content">Напоминаем Вам,
                                                                что {{date('d-m-Y',strtotime($user['start_date']))}} г.
                                                                состоится курс <span style="color: #000;">«{{$user['title']}}» </span> , на который Вы
                                                                зарегистрированы.

                                                                Просьба приехать за 10 минут до начала курса.
                                                                Если вы на личном транспорте, просьба приехать
                                                                заблаговременно, в связи затруднительной парковкой и во
                                                                избежание опоздания.

                                                                <br>Добро пожаловать в Kaizen Center! <br> <span
                                                                        style="color: #000;"><b><br>
Kaizen Center <br>
What’sApp +7 777 676 77 72 </b></span></multiline>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- End Content -->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="50" class="mobile"></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- End Container -->

                    </td>
                </tr>
                <tr>
                    <td align="center">

                        <!-- Start Container -->
                        <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff"
                               style="border-radius:25px" class="container">
                            <tbody>
                            <tr>
                                <td align="center" bgcolor="#E35E31"
                                    style=" border-top-left-radius:4px; border-top-right-radius:4px; font-size: 35px; line-height: 30px; color: #ffffff; font-weight: 700;"></td>
                            </tr>
                            <tr>
                                <td align="center" class="mobile">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#E35E31"
                                           data-bgcolor="FootBoxBG_Color" class="container">
                                        <tbody>
                                        <tr>
                                            <td width="350" align="center" class="mobile"
                                                style=" font-size:12px; line-height:24px;">
                                                <!-- Start Content -->

                                                <!-- End Content -->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- End Container -->

                    </td>
                </tr>
                <tr>
                    <td height="40"></td>
                </tr>
                <tr>
                    <td align="center">

                        <!-- Start Container -->
                        <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
                            <tbody>
                            <tr>
                                <td align="center" class="mobile"
                                    style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:24px;">
                                    <!-- Start Content -->
                                    <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0"
                                           class="container">
                                        <tbody>
                                        <tr>
                                            <td align="center" height="20" class="mobile"
                                                style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #8D8D8D; line-height:23px; font-weight:400;"
                                                data-color="H7 Color" data-size="H7 Size" data-min="10" data-max="24"
                                                mc:edit="">
                                                <multiline>Copyright © 2019 noreply@kaizencenter.kz></multiline>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>                                        <!-- End Container -->
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <!-- End Container -->

                    </td>
                </tr>
                <tr>
                    <td align="center">

                        <!-- Start Container -->
                        <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
                            <tbody>
                            <tr>
                                <td width="400" align="center" class="mobile"
                                    style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:24px;">
                                    <!-- Start Content -->
                                    <table width="380" border="0" align="center" cellpadding="0" cellspacing="0"
                                           class="container">
                                        <tbody>
                                        <tr>
                                            <td align="center" height="20">
                                                <table align="center" width="100%" border="0" cellspacing="0"
                                                       cellpadding="0" class="container">
                                                    <tbody>
                                                    <tr>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>                                        <!-- End Container -->
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <!-- End Container -->

                    </td>
                </tr>
                <tr>
                    <td height="40"></td><!-- Spacer -->
                </tr>
                </tbody>
            </table>


        </td>
    </tr>
</table>
<!-- End Background -->
</body>
</html>
