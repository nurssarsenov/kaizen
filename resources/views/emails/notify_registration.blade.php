<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Клиент прошел регистрацию</title>
</head>
<body>
<h5>Клиенты прошли регистрацию</h5>
<p>На сайте http://kaizencenter.kz/ зарегистрировался новый пользователь </p>
<hr>
@if(count($users))
    @foreach($users as $user)
        <p>ФИО: {{$user['fio'] }}</p>
        <p>Позиция: {{$user['position'] }}</p>
        <p>Компания: {{$user['company'] }}</p>
        <p>Номер телефона: {{$user['phone'] }}</p>
        <p>Город: {{$user['city'] }}</p>
        <p>Email: {{$user['email'] }}</p>
        <p>Курc: {{$user['title']}}</p>
        <p>Дата начала: {{date('d-m-Y',strtotime($user['start_date']))}} г.</p>
        <br>
        <hr>
        <br>
    @endforeach
@endif
</body>
</html>