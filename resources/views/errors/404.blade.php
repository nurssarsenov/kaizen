@extends('web.master')
@section('content')
    <div class="section_container">
        <div class="ui container">
            <div class="section_container__white errors_code">
                <h1 align="center">Ошибка 404. «Страница не найдена»
                    <a href="/#section_courses">назад</a></h1>
            </div>
        </div>
    </div>
@endsection