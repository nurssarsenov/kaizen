@extends('web.master')
@section('content')
    <div class="section_container">
        <div class="ui container">
            <div class="section_container__white errors_code">
                <h1 align="center">Оплата для юр.лиц еще не доступна</h1>
                <a href="{{\URL::previous()}}">назад</a>
            </div>
        </div>
    </div>
@endsection