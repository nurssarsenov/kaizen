<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'Web\IndexController@index');
Route::get('/team/{content_id}', 'Web\IndexController@teamPage');
Route::get('/registration/{course_id}', 'Web\IndexController@courseRegistration')->name('courseRegistration');
Route::get('/registration-members/{course_id}', 'Web\IndexController@courseRegistrationMembers')->name('courseRegistrationMembers');
Route::post('/registration-course', 'Web\IndexController@registrationCourse');
Route::get('/payments', 'Web\IndexController@payments');
Route::get('/payments/success', 'Web\IndexController@paymentsSuccess');
Route::post('/pay', 'Web\PayBoxController@index');
Route::get('/pay/entity', 'Web\PayBoxController@entity');
Route::match(['get','post'],'/pay/entity/ajax', 'Web\PayBoxController@ajax');
Route::get('/pay-atf', 'Web\PayBoxController@payAtf');

Route::match(['get', 'post'], '/payments/ajax', 'Web\IndexController@ajax');
Route::get('/cron/send-notify', 'Web\IndexController@sendNotify');
Route::match(['get', 'post'], '/temp/ajax', 'Web\IndexController@ajax');

Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function () {
    Route::get('login', 'AuthenticateController@login');
    Route::get('logout', 'AuthenticateController@logout');
    Route::match(['get', 'post'], 'ajax', 'AuthenticateController@ajax');
});
Route::group(['middleware' => 'admin', 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('/', 'MainController@index');
    Route::match(['get','post'],'/ajax', 'MainController@ajax');
    Route::get('/export-excel', 'MainController@exportExcel');
    Route::get('/export-client-excel', 'MainController@exportClientExcel');
    Route::get('/users', 'UserController@index');
    Route::match(['get', 'post'], '/users/ajax', 'UserController@ajax');
    Route::get('/blocks', 'BlockController@index');
    Route::match(['get', 'post'], '/blocks/ajax', 'BlockController@ajax');
    Route::get('/contents', 'ContentController@index');
    Route::match(['get', 'post'], '/contents/ajax', 'ContentController@ajax');
    Route::get('/courses', 'CourseController@index');
    Route::match(['get', 'post'], '/courses/ajax', 'CourseController@ajax');
    Route::get('/static-blocks', 'StaticBlockController@index');
    Route::match(['get', 'post'], '/static-blocks/ajax', 'StaticBlockController@ajax');
    Route::get('/client-base','ClientBaseController@index');
    Route::match(['get', 'post'], '/client-base/ajax', 'ClientBaseController@ajax');
});

//Auth::routes();

Route::get('/history', 'UserFuncController@showOrdersHistory')->name('orderHistory');
Route::get('/userLogin', 'UserFuncController@userLogin')->name('userLogin');
Route::get('/resetPassword', 'Auth\AuthenticateController@resetPasswordView');
Route::post('/resetPassword', 'Auth\AuthenticateController@resetPassword')->name('resetPassword');
Route::post('/changePassword', 'Auth\AuthenticateController@changePassword')->name('changePassword');
Route::get('/register', 'Auth\RegisterController@index')->name('userRegister');
Route::post('/register', 'Auth\RegisterController@registerUser')->name('userRegister');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::get('/user/rp/{token}', 'Auth\AuthenticateController@allowResetPassword');
Route::get('/resendVerification', 'Auth\RegisterController@resendVerification')->name('verification.resend');

