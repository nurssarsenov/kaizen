Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '/assets/libs/extjs/ux');
Ext.require(['*', 'Ext.ux.grid.FiltersFeature', 'Ext.window.*', 'Ext.ux.statusbar.StatusBar', 'Ext.selection.CellModel', 'Ext.ux.CheckColumn', 'Ext.tip.*', 'Ext.tree.*']);
Ext.tip.QuickTipManager.init();

Ext.onReady(function () {
    Ext.create('Ext.grid.Panel', {
        id: 'contentsGrid',
        title: 'Список контентов',
        store: Ext.data.StoreManager.lookup('contentsStore'),
        columnLines: true,
        renderTo: 'contentsGrid',
        anchor: '100% 100%',
        height: '80vh',
        features: [{
            ftype: 'filters',
            encode: false,
            local: false,
            filters: []
        },{ftype: 'grouping'}],
        columns: [
            {text: 'ID', width: 60, dataIndex: 'id', filter: {type: 'int'}},
            {text: 'Название', flex: 1, dataIndex: 'title', filter: {type: 'string'}},
            {text: 'Краткое описание', flex: 1, dataIndex: 'sh_content', filter: {type: 'string'}},
            {text: 'Блок', flex: 1, dataIndex: 'block', filter: {type: 'string'}},
            {
                text: 'Состояние', flex: 1, dataIndex: 'is_active', filter: {type: 'int'},
                renderer: function (v) {
                    var state = ['скрытый', 'опубликовано'];
                    return state[v];
                }
            },
            {
                xtype: 'actioncolumn',
                items: [
                    {
                        icls: '',
                        icon: '/assets/admin/img/edit_task.png',
                        tooltip: 'редактировать',
                        handler: function (grid, rowIndex, colIndex) {
                            Ext.create('Ext.window.Window', {
                                title: 'Контент / Редактирование',
                                width: 600,
                                id: 'editContentWindow',
                                modal: true,
                                layout: 'anchor',
                                items: [
                                    Ext.create('Ext.form.Panel', {
                                        id: 'contentsForm',
                                        bodyStyle: {
                                            padding: '10px 20px'
                                        },
                                        defaults: {
                                            allowBlank: true,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            xtype: 'textfield',
                                            margin: '0 0 10 0',
                                            labelAlign: 'top'

                                        },
                                        url: '/admin/contents/ajax?action=editContent',
                                        layout: 'anchor',
                                        items: [
                                            {
                                                name: 'id',
                                                hidden: true
                                            },
                                            {
                                                xtype: 'combobox',
                                                fieldLabel: 'Блок *',
                                                name: 'block_id',
                                                store: Ext.data.StoreManager.lookup('blocksStore'),
                                                valueField: 'id',
                                                editable: false,
                                                displayField: 'title',
                                                queryMode: 'local',
                                                emptyText: 'Выберите тип...',
                                                allowBlank: false
                                            },
                                            {
                                                fieldLabel: 'Название *',
                                                name: 'title',
                                                allowBlank: false
                                            },
                                            {
                                                fieldLabel: 'Краткое описание  *',
                                                name: 'sh_content'
                                            },
                                            {
                                                xtype: 'textarea',
                                                fieldLabel: 'Описание  *',
                                                name: 'content'
                                            },
                                            {
                                                xtype: 'checkbox',
                                                fieldLabel: 'Опубликовать*',
                                                name: 'is_active',
                                                labelAlign: 'left',
                                                checked: true,
                                                inputValue: 1,
                                                uncheckedValue: 0
                                            },
                                            {
                                                xtype: 'filefield',
                                                fieldLabel: 'Картинка *',
                                                name: 'img',
                                                editable: false
                                            },
                                            {
                                                xtype: 'box',
                                                margin: '5 5 5 15',
                                                html: Ext.getCmp('contentsGrid').getStore().getAt(rowIndex).data.img
                                            },
                                            {
                                                xtype: 'filefield',
                                                fieldLabel: 'Портфолио *',
                                                name: 'pdf_file',
                                                editable: false
                                            },
                                            {
                                                xtype: 'box',
                                                margin: '5 5 5 15',
                                                html: Ext.getCmp('contentsGrid').getStore().getAt(rowIndex).data.pdf_file
                                            }
                                        ],
                                        buttons: [
                                            {
                                                text: 'Очистить',
                                                handler: function () {
                                                    this.up('form').getForm().reset();
                                                }
                                            },
                                            {
                                                text: 'Сохранить',
                                                formBind: true,
                                                disabled: true,
                                                handler: function () {
                                                    var form = this.up('form').getForm();
                                                    if (form.isValid()) {
                                                        form.submit({
                                                            params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                            success: function (form, action) {
                                                                Ext.getCmp('contentsGrid').getStore().load();
                                                                Ext.getCmp('editContentWindow').close();
                                                            },
                                                            failure: function (form, action) {
                                                                var res = JSON.parse(action.response.responseText);
                                                                console.log(res);
                                                                if (!res.status) {
                                                                    var msg = '';
                                                                    $.each(res.errors, function (i, val) {
                                                                        var br = '';
                                                                        if (i !== 0) br = '<br>';
                                                                        msg += br + val;
                                                                    });
                                                                    Ext.Msg.alert('Ошибка', msg);
                                                                }

                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        ]
                                    })
                                ]
                            }).show();
                            var rec = Ext.getCmp('contentsGrid').getStore().getAt(rowIndex);
                            var panel = Ext.getCmp('contentsForm');
                            panel.getForm().loadRecord(rec);
                        }
                    },
                    {
                        icon: '/assets/admin/img/delete_task.png',
                        tooltip: 'удалить',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = Ext.getCmp('contentsGrid').getStore().getAt(rowIndex);
                            Ext.Ajax.request({
                                url: '/admin/contents/ajax?action=deleteContent',
                                method: 'POST',
                                params: {_token: $('meta[name="csrf-token"]').attr('content'), id: rec.data.id},
                                success: function (response) {
                                    var values = Ext.decode(response.responseText);
                                    if (values.success) {
                                        Ext.getCmp('contentsGrid').getStore().load();
                                    }
                                },
                                failure: function (response) {
                                    var res = Ext.decode(response.responseText);
                                    if (!res.status) {
                                        var msg = '';
                                        $.each(res.errors, function (i, val) {
                                            var br = '';
                                            if (i !== 0) br = '<br>';
                                            msg += br + val;
                                        });
                                        Ext.Msg.alert('Ошибка', msg);
                                    }
                                }
                            });
                        }
                    }
                ]
            }
        ],
        listeners: {
            select: function () {
                Ext.getCmp('contentsGrid').getStore().getProxy().extraParams._token = $('meta[name="csrf-token"]').attr('content');
            }
        },
        tbar: [
            {
                xtype: 'textfield',
                width: 350,
                emptyText: 'Найти контента',
                listeners: {
                    change: function (data, record) {
                        var value = record;
                        var grid = Ext.getCmp('contentsGrid'),
                            filter = grid.filters.getFilter('title');

                        if (!filter) {
                            filter = grid.filters.addFilter({
                                active: true,
                                type: 'string',
                                dataIndex: 'title'
                            });

                            filter.menu.show();
                            filter.setValue(value);
                            filter.menu.hide();
                        } else {
                            filter.setValue(value);
                            filter.setActive(true);
                        }
                    }
                }
            },
            '->',
            {
                text: 'Добавить',
                scope: this,
                handler: function () {
                    Ext.create('Ext.window.Window', {
                        title: 'Блоки',
                        id: 'createContent',
                        width: 600,
                        modal: true,
                        layout: 'anchor',
                        items: [
                            Ext.create('Ext.form.Panel', {
                                id: 'contentsForm',
                                bodyStyle: {
                                    padding: '10px 20px'
                                },
                                defaults: {
                                    allowBlank: true,
                                    msgTarget: 'side',
                                    anchor: '100%',
                                    xtype: 'textfield',
                                    margin: '0 0 10 0',
                                    labelAlign: 'top'

                                },
                                url: '/admin/contents/ajax?action=addContent',
                                layout: 'anchor',
                                items: [
                                    {
                                        xtype: 'combobox',
                                        fieldLabel: 'Блок *',
                                        name: 'block_id',
                                        store: Ext.data.StoreManager.lookup('blocksStore'),
                                        valueField: 'id',
                                        editable: false,
                                        displayField: 'title',
                                        queryMode: 'local',
                                        emptyText: 'Выберите тип...',
                                        allowBlank: false
                                    },
                                    {
                                        fieldLabel: 'Название *',
                                        name: 'title',
                                        allowBlank: false
                                    },
                                    {
                                        fieldLabel: 'Краткое описание  *',
                                        name: 'sh_content'
                                    },
                                    {
                                        xtype: 'textarea',
                                        fieldLabel: 'Описание  *',
                                        name: 'content'
                                    },
                                    {
                                        xtype: 'checkbox',
                                        fieldLabel: 'Опубликовать*',
                                        name: 'is_active',
                                        labelAlign: 'left',
                                        checked: true,
                                        inputValue: 1,
                                        uncheckedValue: 0
                                    },
                                    {
                                        xtype: 'filefield',
                                        fieldLabel: 'Картинка *',
                                        name: 'img',
                                        editable: false
                                    },
                                    {
                                        xtype: 'filefield',
                                        fieldLabel: 'Портфолио *',
                                        name: 'pdf_file',
                                        editable: false
                                    }
                                ],
                                buttons: [
                                    {
                                        text: 'Очистить',
                                        handler: function () {
                                            this.up('form').getForm().reset();
                                        }
                                    },
                                    {
                                        text: 'Сохранить',
                                        formBind: true,
                                        disabled: true,
                                        handler: function () {
                                            var form = this.up('form').getForm();
                                            if (form.isValid()) {
                                                form.submit({
                                                    params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                    success: function (form, action) {
                                                        Ext.getCmp('contentsForm').getForm().reset();
                                                        Ext.getCmp('contentsGrid').getStore().load();
                                                    },
                                                    failure: function (form, action) {
                                                        var res = JSON.parse(action.response.responseText);
                                                        if (!res.status) {
                                                            var msg = '';
                                                            $.each(res.errors, function (i, val) {
                                                                var br = '';
                                                                if (i !== 0) br = '<br>';
                                                                msg += br + val;
                                                            });
                                                            Ext.Msg.alert('Ошибка', msg);
                                                        }

                                                    }
                                                });
                                            }
                                        }
                                    }
                                ]
                            })
                        ]
                    }).show();
                }
            }
        ],
        bbar: [
            Ext.create('Ext.PagingToolbar', {
                pageSize: 50,
                store: Ext.data.StoreManager.lookup('contentsStore'),
                displayInfo: true
            })
        ],
        plugins: [],
        viewConfig: {
            loadMask: {
                msg: 'загрузка...'
            }
        }
    });
});
Ext.create('Ext.data.JsonStore', {
    storeId: 'contentsStore',
    autoLoad: true,
    fields: ['id', 'title', 'img', 'sh_content', 'content', 'is_active', 'block', 'block_id', 'pdf_file'],
    pageSize: 30,
    groupField: 'block',
    remoteSort: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/admin/contents/ajax?action=contents'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    },
    sorters: [
        {property: 'id', direction: 'DESC'}
    ]
});

Ext.create('Ext.data.JsonStore', {
    storeId: 'blocksStore',
    autoLoad: true,
    fields: ['id', 'title'],
    pageSize: 30,
    remoteSort: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/admin/contents/ajax?action=getBlocks'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    },
    sorters: [
        {property: 'id', direction: 'DESC'}
    ]
});

