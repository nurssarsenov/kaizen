Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '/assets/libs/extjs/ux');
Ext.require(['*', 'Ext.ux.grid.FiltersFeature', 'Ext.window.*', 'Ext.ux.statusbar.StatusBar', 'Ext.selection.CellModel', 'Ext.ux.CheckColumn', 'Ext.tip.*', 'Ext.tree.*']);
Ext.tip.QuickTipManager.init();
Ext.apply(Ext.form.VTypes, {
    password: function (val, field) {
        if (field.initialPassField) {
            var pwd = Ext.getCmp(field.initialPassField);
            return (val == pwd.getValue());
        }
        return true;
    },
    phone: function () {
        var re = /^(([(])([0-9]){3})([)])([-])([0-9]{3})([-])([0-9]{2})([-])([0-9]{2})$/;
        return function (v) {
            return re.test(v);
        };
    }()
});
Ext.onReady(function () {
    Ext.create('Ext.grid.Panel', {
        id: 'usersGrid',
        title: 'Список пользователей',
        store: Ext.data.StoreManager.lookup('usersStore'),
        columnLines: true,
        renderTo: 'usersGrid',
        anchor: '100% 100%',
        height: '80vh',
        features: [{
            ftype: 'filters',
            encode: false,
            local: false,
            filters: []
        }],
        columns: [
            {text: 'ID', width: 60, dataIndex: 'id', filter: {type: 'int'}},
            {text: 'ФИО', flex: 1, dataIndex: 'fio', filter: {type: 'string'}},
            {text: 'Email', flex: 1, dataIndex: 'email', filter: {type: 'string'}},
            {text: 'Телефон', flex: 1, dataIndex: 'phone', filter: {type: 'string'}},
            {text: 'Город', flex: 1, dataIndex: 'city', filter: {type: 'string'}},
            {text: 'Компания', flex: 1, dataIndex: 'company', filter: {type: 'string'}},
            {text: 'Должность', flex: 1, dataIndex: 'position', filter: {type: 'string'}},
            {
                text: 'Роль', flex: 1, dataIndex: 'is_role', filter: {type: 'int'},
                renderer: function (a) {
                    var roles = ['guest', 'admin'];
                    return roles[a];
                }
            },
            {
                xtype: 'actioncolumn',
                items: [
                    {
                        icls: '',
                        icon: '/assets/admin/img/edit_task.png',
                        tooltip: 'редактировать',
                        handler: function (grid, rowIndex, colIndex) {
                            Ext.create('Ext.window.Window', {
                                title: 'Пользователи / Редактирование',
                                width: 600,
                                id: 'editRegisterWindow',
                                modal: true,
                                layout: 'anchor',
                                items: [
                                    Ext.create('Ext.form.Panel', {
                                        id: 'editRegisterForm',
                                        bodyStyle: {
                                            padding: '10px 20px'
                                        },
                                        defaults: {
                                            allowBlank: true,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            xtype: 'textfield',
                                            margin: '0 0 10 0',
                                            labelAlign: 'top'

                                        },
                                        url: '/admin/users/ajax?action=editUser',
                                        layout: 'anchor',
                                        defaultType: 'textfield',
                                        items: [
                                            {
                                                name: 'id',
                                                hidden: true
                                            },
                                            {
                                                fieldLabel: 'ФИО *',
                                                name: 'fio',
                                                labelAlign: 'top',
                                                allowBlank: false
                                            },
                                            {
                                                fieldLabel: 'Город',
                                                name: 'city'
                                            },
                                            {
                                                fieldLabel: 'Должность',
                                                name: 'position'
                                            },
                                            {
                                                fieldLabel: 'Компания',
                                                name: 'company'
                                            },
                                            {
                                                vtype: 'email',
                                                fieldLabel: 'Email *',
                                                name: 'email',
                                                allowBlank: false
                                            },
                                            {
                                                vtype: 'phone',
                                                fieldLabel: 'Телефон *',
                                                name: 'phone',
                                                allowBlank: false,
                                                emptyText: '(xxxx)-xxx-xx-xx'
                                            },
                                            {
                                                inputType: 'password',
                                                fieldLabel: 'Пароль *',
                                                name: 'password',
                                                id: 'password'
                                            },
                                            {
                                                inputType: 'password',
                                                fieldLabel: 'Повтор пароля *',
                                                name: 'password_confirmation',
                                                vtype: 'password',
                                                initialPassField: 'password'
                                            },
                                            {
                                                xtype: 'combobox',
                                                fieldLabel: 'Роль *',
                                                name: 'is_role',
                                                store: roles,
                                                valueField: 'id',
                                                editable: false,
                                                displayField: 'role',
                                                queryMode: 'local',
                                                emptyText: 'Выберите роль...',
                                                allowBlank: false
                                            }
                                        ],
                                        buttons: [{
                                            text: 'Очистить',
                                            handler: function () {
                                                this.up('form').getForm().reset();
                                            }
                                        },
                                            {
                                                text: 'Сохранить',
                                                formBind: true,
                                                disabled: true,
                                                handler: function () {
                                                    var form = this.up('form').getForm();
                                                    if (form.isValid()) {
                                                        form.submit({
                                                            params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                            success: function (form, action) {
                                                                Ext.getCmp('usersGrid').getStore().load();
                                                                Ext.getCmp('editRegisterWindow').close();
                                                            },
                                                            failure: function (form, action) {
                                                                var res = JSON.parse(action.response.responseText);
                                                                console.log(res);
                                                                if (!res.status) {
                                                                    var msg = '';
                                                                    $.each(res.errors, function (i, val) {
                                                                        var br = '';
                                                                        if (i !== 0) br = '<br>';
                                                                        msg += br + val;
                                                                    });
                                                                    Ext.Msg.alert('Ошибка', msg);
                                                                }

                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        ]
                                    })
                                ]
                            }).show();
                            var rec = Ext.getCmp('usersGrid').getStore().getAt(rowIndex);
                            var panel = Ext.getCmp('editRegisterForm');
                            panel.getForm().loadRecord(rec);
                            $('input[name=phone]').mask('(000)-000-00-00');
                        }
                    },
                    {
                        icon: '/assets/admin/img/delete_task.png',
                        tooltip: 'удалить',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = Ext.getCmp('usersGrid').getStore().getAt(rowIndex);
                            Ext.Ajax.request({
                                url: '/admin/users/ajax?action=deleteUser',
                                method: 'POST',
                                params: {_token: $('meta[name="csrf-token"]').attr('content'), id: rec.data.id},
                                success: function (response) {
                                    var values = Ext.decode(response.responseText);
                                    if (values.success) {
                                        Ext.getCmp('usersGrid').getStore().load();
                                    }
                                },
                                failure: function (response) {
                                    var res = Ext.decode(response.responseText);
                                    if (!res.status) {
                                        var msg = '';
                                        $.each(res.errors, function (i, val) {
                                            var br = '';
                                            if (i !== 0) br = '<br>';
                                            msg += br + val;
                                        });
                                        Ext.Msg.alert('Ошибка', msg);
                                    }
                                }
                            });
                        }
                    }
                ]
            }
        ],
        listeners: {
            select: function () {
                Ext.getCmp('usersGrid').getStore().getProxy().extraParams._token = $('meta[name="csrf-token"]').attr('content');
            }
        },
        tbar: [
            {
                xtype: 'textfield',
                width: 350,
                emptyText: 'Найти пользователя',
                listeners: {
                    change: function (data, record) {
                        var value = record;
                        var grid = Ext.getCmp('usersGrid'),
                            filter = grid.filters.getFilter('fio');

                        if (!filter) {
                            filter = grid.filters.addFilter({
                                active: true,
                                type: 'string',
                                dataIndex: 'fio'
                            });

                            filter.menu.show();
                            filter.setValue(value);
                            filter.menu.hide();
                        } else {
                            filter.setValue(value);
                            filter.setActive(true);
                        }
                    }
                }
            },
            '->',
            {
                text: 'Добавить',
                scope: this,
                handler: function () {
                    Ext.create('Ext.window.Window', {
                        title: 'Пользователи',
                        id: 'createUser',
                        width: 600,
                        modal: true,
                        layout: 'anchor',
                        items: [
                            Ext.create('Ext.form.Panel', {
                                id: 'registerForm',
                                bodyStyle: {
                                    padding: '10px 20px'
                                },
                                defaults: {
                                    allowBlank: true,
                                    msgTarget: 'side',
                                    anchor: '100%',
                                    xtype: 'textfield',
                                    margin: '0 0 10 0',
                                    labelAlign: 'top'

                                },
                                url: '/admin/users/ajax?action=addUser',
                                layout: 'anchor',
                                defaultType: 'textfield',
                                items: [
                                    {
                                        fieldLabel: 'ФИО *',
                                        name: 'fio',
                                        labelAlign: 'top',
                                        allowBlank: false
                                    },
                                    {
                                        fieldLabel: 'Город',
                                        name: 'city'
                                    },
                                    {
                                        fieldLabel: 'Должность',
                                        name: 'position'
                                    },
                                    {
                                        fieldLabel: 'Компания',
                                        name: 'company'
                                    },
                                    {
                                        vtype: 'email',
                                        fieldLabel: 'Email *',
                                        name: 'email',
                                        allowBlank: false
                                    },
                                    {
                                        vtype: 'phone',
                                        fieldLabel: 'Телефон *',
                                        name: 'phone',
                                        allowBlank: false,
                                        emptyText: '(xxx)-xxx-xx-xx'
                                    },
                                    {
                                        inputType: 'password',
                                        fieldLabel: 'Пароль *',
                                        name: 'password',
                                        allowBlank: false,
                                        id: 'password'
                                    },
                                    {
                                        inputType: 'password',
                                        fieldLabel: 'Повтор пароля *',
                                        name: 'password_confirmation',
                                        allowBlank: false,
                                        vtype: 'password',
                                        initialPassField: 'password'
                                    },
                                    {
                                        xtype: 'combobox',
                                        fieldLabel: 'Роль *',
                                        name: 'is_role',
                                        store: roles,
                                        valueField: 'id',
                                        editable: false,
                                        displayField: 'role',
                                        queryMode: 'local',
                                        emptyText: 'Выберите роль...',
                                        allowBlank: false
                                    }
                                ],
                                buttons: [
                                    {
                                        text: 'Очистить',
                                        handler: function () {
                                            this.up('form').getForm().reset();
                                        }
                                    },
                                    {
                                        text: 'Сохранить',
                                        formBind: true,
                                        disabled: true,
                                        handler: function () {
                                            var form = this.up('form').getForm();
                                            if (form.isValid()) {
                                                form.submit({
                                                    params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                    success: function (form, action) {
                                                        Ext.getCmp('registerForm').getForm().reset();
                                                        Ext.getCmp('usersGrid').getStore().load();
                                                        console.log('success');
                                                    },
                                                    failure: function (form, action) {
                                                        var res = JSON.parse(action.response.responseText);
                                                        if (!res.status) {
                                                            var msg = '';
                                                            $.each(res.errors, function (i, val) {
                                                                var br = '';
                                                                if (i !== 0) br = '<br>';
                                                                msg += br + val;
                                                            });
                                                            Ext.Msg.alert('Ошибка', msg);
                                                        }

                                                    }
                                                });
                                            }
                                        }
                                    }
                                ]
                            })
                        ]
                    }).show();
                    $('input[name=phone]').mask('(000)-000-00-00');
                }
            }
        ],
        bbar: [
            Ext.create('Ext.PagingToolbar', {
                pageSize: 50,
                store: Ext.data.StoreManager.lookup('usersStore'),
                displayInfo: true
            })
        ],
        plugins: [],
        viewConfig: {
            loadMask: {
                msg: 'загрузка...'
            }
        }
    });
});
Ext.create('Ext.data.JsonStore', {
    storeId: 'usersStore',
    autoLoad: true,
    fields: ['id', 'fio', 'email', 'phone', 'is_role', 'city', 'company', 'position'],
    pageSize: 30,
    remoteSort: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/admin/users/ajax?action=users'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    },
    sorters: [
        {property: 'id', direction: 'DESC'}
    ]
});
var roles = Ext.create('Ext.data.Store', {
    fields: ['id', 'role'],
    data: [
        {"id": 0, "role": "guest"},
        {"id": 1, "role": "admin"}
    ]
});


