Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', 'assets/libs/extjs/ux');
Ext.require(['*', 'Ext.ux.grid.FiltersFeature', 'Ext.window.*', 'Ext.ux.statusbar.StatusBar', 'Ext.selection.CellModel', 'Ext.ux.CheckColumn', 'Ext.tip.*', 'Ext.tree.*']);
Ext.tip.QuickTipManager.init();
Ext.apply(Ext.form.VTypes, {
    password: function (val, field) {
        if (field.initialPassField) {
            var pwd = Ext.getCmp(field.initialPassField);
            return (val == pwd.getValue());
        }
        return true;
    },
    phone: function () {
        var re = /^(([(])([0-9]){3})([)])([-])([0-9]{3})([-])([0-9]{2})([-])([0-9]{2})$/;
        return function (v) {
            return re.test(v);
        };
    }()
});
Ext.onReady(function () {
    Ext.create('Ext.grid.Panel', {
        id: 'ordersGrid',
        title: 'Список заказов',
        store: Ext.data.StoreManager.lookup('ordersStore'),
        columnLines: true,
        renderTo: 'ordersGrid',
        anchor: '100% 100%',
        height: '80vh',
        features: [{
            ftype: 'filters',
            encode: false,
            local: false,
            filters: []
        }],
        columns: [
            {text: 'ID', width: 60, dataIndex: 'id', filter: {type: 'int'}},
            {text: 'ФИО', flex: 1, dataIndex: 'fio', filter: {type: 'string'}},
			{
                text: 'Роль', width: 100, dataIndex: 'is_company_user', filter: {type: 'int'},
                renderer: function (a) {
                    var user_type = ['Физ. лицо', 'Юр. лицо'];
                    return user_type[a];
                }
            },
            {text: 'Email', flex: 1, dataIndex: 'email', filter: {type: 'string'}},
            {text: 'Телефон', flex: 1, dataIndex: 'phone', filter: {type: 'string'}},
            {text: 'Город', flex: 1, dataIndex: 'city', filter: {type: 'string'}},
            {text: 'Компания', flex: 1, dataIndex: 'company', filter: {type: 'string'}},
            {text: 'Должность', flex: 1, dataIndex: 'position', filter: {type: 'string'}},
            {text: 'Курс', width: 310, dataIndex: 'course_title', filter: {type: 'decimal'}},
            {text: 'Цена', width: 100, dataIndex: 'price'},
            {text: 'Дата заявки', flex: 1, dataIndex: 'created_at'},
            {
                text: 'Статус', width: 100, dataIndex: 'is_paid', filter: {type: 'int'},
                renderer: function (a, meta, record, rowIndex) {
					var is_comp_user = record.get('is_company_user');
					if(is_comp_user === 1){
						return "";
					}
					else{
						var paid = ['<span style="color: red;">не оплачено</span>', '<span style="color: green;">оплачено</span>'];
						return paid[a];
					}
                }
            },
            {text: 'Присутствие', flex:1, dataIndex: 'is_came', filter: {type: 'int'},
                renderer: function (a, meta, record, rowIndex) {
                    var is_comp_user = record.get('is_company_user');
                    if(is_comp_user === 1){
                        return "";
                    }
                    else{
                        if (a === null) {
                            return "";
                        }
                        var paid = ['<span style="color: red;">не был</span>', '<span style="color: green;">был</span>'];
                        return paid[a];
                    }
                }
            },
            {
                text: 'Счет на оплату', width: 120, dataIndex: 'entities_id', filter: {type: 'int'},
                renderer: function (a) {
                    var paid = '<a href="/pay/entity/ajax?action=generate&entity=' + a + '" target="_blank">скачать</a>'
                    if (!a) {
                        return a;
                    }
                    return paid;
                }
            },
            {
                text: 'Акт', width: 80, dataIndex: 'entities_id', filter: {type: 'int'},
                renderer: function (a) {
                    var paid = '<a href="/pay/entity/ajax?action=generate-akt&entity=' + a + '" target="_blank">скачать</a>'
                    if (!a) {
                        return a;
                    }
                    return paid;
                }
            },
            {
                xtype: 'actioncolumn',
                width: 80,
                items: [
                    {
                        icls: '',
                        icon: '/assets/admin/img/edit_task.png',
                        tooltip: 'редактировать',
                        handler: function (grid, rowIndex, colIndex) {
                            Ext.create('Ext.window.Window', {
                                title: 'Заказы / Редактирование',

                                width: 600,
                                id: 'editOrderWindow',
                                modal: true,
                                layout: 'anchor',
                                items: [
                                    Ext.create('Ext.form.Panel', {
                                        id: 'editOrderForm',
                                        bodyStyle: {
                                            padding: '10px 20px'
                                        },
                                        defaults: {
                                            allowBlank: true,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            xtype: 'textfield',
                                            margin: '0 0 10 0',
                                            labelAlign: 'top'

                                        },
                                        url: '/admin/ajax?action=editOrder',
                                        layout: 'anchor',
                                        defaultType: 'textfield',
                                        items: [
                                            {
                                                name: 'id',
                                                hidden: true
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'ФИО',
                                                name: 'fio',
                                                labelAlign: 'top',
                                            },
                                            {
                                                xtype: 'combobox',
                                                fieldLabel: 'Статус *',
                                                name: 'is_paid',
                                                store: statuses,
                                                valueField: 'id',
                                                displayField: 'role',
                                                editable: false,
                                                queryMode: 'local',
                                                emptyText: 'Выберите статус...',
                                                allowBlank: true
                                            },
                                            {
                                                xtype: 'combobox',
                                                fieldLabel: 'Присутствие *',
                                                name: 'is_came',
                                                store: present_statuses,
                                                valueField: 'id',
                                                displayField: 'role',
                                                editable: false,
                                                queryMode: 'local',
                                                emptyText: 'Выберите присутствие...',
                                                allowBlank: true
                                            },
                                        ],
                                        buttons: [{
                                            text: 'Очистить',
                                            handler: function () {
                                                this.up('form').getForm().reset();
                                            }
                                        },
                                            {
                                                text: 'Сохранить',
                                                formBind: true,
                                                disabled: true,
                                                handler: function () {
                                                    var form = this.up('form').getForm();
                                                    if (form.isValid()) {
                                                        form.submit({
                                                            params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                            success: function (form, action) {
                                                                Ext.getCmp('ordersGrid').getStore().load();
                                                                Ext.getCmp('editOrderWindow').close();
                                                            },
                                                            failure: function (form, action) {
                                                                var res = JSON.parse(action.response.responseText);
                                                                console.log(res);
                                                                if (!res.status) {
                                                                    var msg = '';
                                                                    $.each(res.errors, function (i, val) {
                                                                        var br = '';
                                                                        if (i !== 0) br = '<br>';
                                                                        msg += br + val;
                                                                    });
                                                                    Ext.Msg.alert('Ошибка', msg);
                                                                }

                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        ]
                                    })
                                ]
                            }).show();
                            var rec = Ext.getCmp('ordersGrid').getStore().getAt(rowIndex);
                            var panel = Ext.getCmp('editOrderForm');
                            panel.getForm().loadRecord(rec);
                            $('input[name=phone]').mask('(000)-000-00-00');
                        }
                    },

                    {
                        icon: '/assets/admin/img/delete_task.png',
                        tooltip: 'удалить',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = Ext.getCmp('ordersGrid').getStore().getAt(rowIndex);
                            Ext.Ajax.request({
                                url: '/admin/ajax?action=delete_order',
                                method: 'POST',
                                params: {_token: $('meta[name="csrf-token"]').attr('content'), id: rec.data.id},
                                success: function (response) {
                                    var values = Ext.decode(response.responseText);
                                    if (values.success) {
                                        Ext.getCmp('ordersGrid').getStore().load();
                                    }
                                },
                                failure: function (response) {
                                    var res = Ext.decode(response.responseText);
                                    if (!res.status) {
                                        var msg = '';
                                        $.each(res.errors, function (i, val) {
                                            var br = '';
                                            if (i !== 0) br = '<br>';
                                            msg += br + val;
                                        });
                                        Ext.Msg.alert('Ошибка', msg);
                                    }
                                }
                            });
                        }
                    }
                ]
            },
        ],
        listeners: {
            select: function () {
                Ext.getCmp('ordersGrid').getStore().getProxy().extraParams._token = $('meta[name="csrf-token"]').attr('content');
            }
        },

        tbar: [
            {
                xtype: 'textfield',
                width: 350,
                emptyText: 'Найти заказ',
                listeners: {
                    change: function (data, record) {
                        var value = record;

                        var grid = Ext.getCmp('ordersGrid'),
                            filter = grid.filters.getFilter('fio');

                        if (!filter) {
                            filter = grid.filters.addFilter({
                                active: true,
                                type: 'string',
                                dataIndex: 'fio'
                            });

                            filter.menu.show();
                            filter.setValue(value);
                            filter.menu.hide();
                        } else {
                            filter.setValue(value);
                            filter.setActive(true);
                        }

                    },

                }
            },
            '->',
            {
                xtype: 'button',
                tooltip: 'Экспортировать',
                text: 'Скачать Excel',
                margin: '0 30 0 0',
                handler: function () {
                    console.log('sdsd');
                    Ext.getCmp('export-excel').show();
                }
            }
        ],
        bbar: [
            Ext.create('Ext.PagingToolbar', {
                pageSize: 50,
                store: Ext.data.StoreManager.lookup('ordersStore'),
                displayInfo: true
            })
        ],
        plugins: [],
        viewConfig: {
            loadMask: {
                msg: 'загрузка...'
            }
        }
    });
});
Ext.create('Ext.data.JsonStore', {
    storeId: 'ordersStore',
    autoLoad: true,
    fields: ['id', 'fio', 'is_company_user', 'email', 'phone', 'city', 'company', 'position', 'course_title', 'created_at', 'is_paid', 'start_date', 'price', 'entities_id', 'is_came'],
    pageSize: 30,
    remoteSort: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/admin/ajax?action=orders'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    },
    sorters: [
        {property: 'id', direction: 'DESC'}
    ]
});
Ext.create('widget.window', {
    title: 'Экспортировать в excel',
    closable: true,
    id: 'export-excel',
    resizable: false,
    modal: true,
    closeAction: 'hide',
    layout: 'anchor',
    width: 300,
    height: 150,
    items: [
        {
            xtype: 'form',
            id: 'export-orders',
            anchor: '100% 100%',
            url: '/admin/export-excel',
            layout: 'anchor',
            standardSubmit: true,
            method: 'GET',
            fieldDefaults: {
                margin: '5 5 10 5',
                anchor: '100%',
                submitFormat: 'Y-m-d',
                format: 'd.m.Y',
                allowBlank: false
            },
            items: [
                {
                    layout: 'hbox',
                    border: false,
                    margin: '10 0 0 0',
                    items: [
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Период',
                            labelWidth: 60,
                            width: 165,
                            name: 'start_date'
                        },
                        {
                            xtype: 'datefield',
                            width: 100,
                            name: 'end_date'
                        }
                    ]
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Экспортировать',
            handler: function () {
                var form = Ext.getCmp('export-orders').getForm();
                if (form.isValid()) {
                    this.up().up().close();
                    form.submit();
                }
                else {
                    Ext.MessageBox.alert('Ошибка', 'Вы не указали период');
                }
            }
        },
        {
            text: 'Закрыть',
            handler: function () {
                this.up().up().close();
            }
        }
    ]
});
var statuses = Ext.create('Ext.data.Store', {
    fields: ['id', 'role'],
    data: [
        {"id": 0, "role": "Не оплачено"},
        {"id": 1, "role": "Оплачено"}
    ]
});
var present_statuses = Ext.create('Ext.data.Store', {
    fields: ['id', 'role'],
    data: [
        {"id": 0, "role": "Не был"},
        {"id": 1, "role": "был"}
    ]
});