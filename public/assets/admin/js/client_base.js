Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '/assets/libs/extjs/ux');
Ext.require(['*', 'Ext.ux.grid.FiltersFeature', 'Ext.window.*', 'Ext.ux.statusbar.StatusBar', 'Ext.selection.CellModel', 'Ext.ux.CheckColumn', 'Ext.tip.*', 'Ext.tree.*']);
Ext.tip.QuickTipManager.init();

Ext.onReady(function () {
    Ext.create('Ext.grid.Panel', {
        id: 'clientBaseGrid',
        title: 'База клиентов',
        store: Ext.data.StoreManager.lookup('clientBaseStore'),
        columnLines: true,
        renderTo: 'clientBaseGrid',
        anchor: '100% 100%',
        height: '80vh',
        features: [{
            ftype: 'filters',
            encode: false,
            local: false,
            filters: []
        }],
        columns: [
            {text: 'ID', width: 60, dataIndex: 'id', filter: {type: 'int'}},
            {text: 'Email', flex: 1, dataIndex: 'email', filter: {type: 'string'}},
        ],
        listeners: {
            select: function () {
                Ext.getCmp('clientBaseGrid').getStore().getProxy().extraParams._token = $('meta[name="csrf-token"]').attr('content');
            }
        },
        tbar: [
            {
                xtype: 'textfield',
                width: 350,
                emptyText: 'Найти email',
                listeners: {
                    change: function (data, record) {
                        var value = record;
                        var grid = Ext.getCmp('clientBaseGrid'),
                            filter = grid.filters.getFilter('email');

                        if (!filter) {
                            filter = grid.filters.addFilter({
                                active: true,
                                type: 'string',
                                dataIndex: 'email'
                            });

                            filter.menu.show();
                            filter.setValue(value);
                            filter.menu.hide();
                        } else {
                            filter.setValue(value);
                            filter.setActive(true);
                        }
                    }
                }
            },			 '->',			{                xtype: 'button',                tooltip: 'Экспортировать',                text: 'Скачать Excel',                margin: '0 30 0 0',                handler: function () {                    console.log('sdsd');                    Ext.getCmp('export-excel').show();                }            }
        ],
        bbar: [
            Ext.create('Ext.PagingToolbar', {
                pageSize: 50,
                store: Ext.data.StoreManager.lookup('clientBaseStore'),
                displayInfo: true
            })
        ],
        plugins: [],
        viewConfig: {
            loadMask: {
                msg: 'загрузка...'
            }
        }
    });
});
Ext.create('Ext.data.JsonStore', {
    storeId: 'clientBaseStore',
    autoLoad: true,
    fields: ['id', 'email'],
    pageSize: 30,
    remoteSort: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/admin/client-base/ajax?action=clients'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    },
    sorters: [
        {property: 'id', direction: 'DESC'}
    ]
});Ext.create('widget.window', {    title: 'Экспортировать в excel',    closable: true,    id: 'export-excel',    resizable: false,    modal: true,    closeAction: 'hide',    layout: 'anchor',    width: 300,    height: 150,    items: [        {            xtype: 'form',            id: 'export-clients',            anchor: '100% 100%',            url: '/admin/export-client-excel',            layout: 'anchor',            standardSubmit: true,            method: 'GET',            fieldDefaults: {                margin: '5 5 10 5',                anchor: '100%',                submitFormat: 'Y-m-d',                format: 'd.m.Y',                allowBlank: false            }        }    ],    buttons: [        {            text: 'Экспортировать',            handler: function () {                var form = Ext.getCmp('export-clients').getForm();                this.up().up().close();				form.submit();            }        },        {            text: 'Закрыть',            handler: function () {                this.up().up().close();            }        }    ]});


