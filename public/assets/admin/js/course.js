Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '/assets/libs/extjs/ux');
Ext.require(['*', 'Ext.ux.grid.FiltersFeature', 'Ext.window.*', 'Ext.ux.statusbar.StatusBar', 'Ext.selection.CellModel', 'Ext.ux.CheckColumn', 'Ext.tip.*', 'Ext.tree.*']);
Ext.tip.QuickTipManager.init();

Ext.onReady(function () {
    Ext.create('Ext.grid.Panel', {
        id: 'coursesGrid',
        title: 'Список курсов',
        store: Ext.data.StoreManager.lookup('coursesStore'),
        columnLines: true,
        renderTo: 'coursesGrid',
        anchor: '100% 100%',
        height: '80vh',
        features: [{
            ftype: 'filters',
            encode: false,
            local: false,
            filters: []
        }],
        columns: [
            {text: 'ID', width: 60, dataIndex: 'id', filter: {type: 'int'}},
            {text: 'Название', flex: 1, dataIndex: 'title', filter: {type: 'string'}},
            {text: 'Краткое описание', flex: 1, dataIndex: 'sh_content', filter: {type: 'string'}},
            {text: 'Дата началы', flex: 1, dataIndex: 'start_date_string', filter: {type: 'datetime'}},
            {text: 'Неделя', flex: 1, dataIndex: 'week', filter: {type: 'string'}},
            {text: 'Цена', flex: 1, dataIndex: 'price', filter: {type: 'datetime'}},
            {
                text: 'Состояние', flex: 1, dataIndex: 'is_active', filter: {type: 'int'},
                renderer: function (v) {
                    var state = ['скрытый', 'опубликовано'];
                    return state[v];
                }
            },
            {
                xtype: 'actioncolumn',
                items: [
                    {
                        icls: '',
                        icon: '/assets/admin/img/edit_task.png',
                        tooltip: 'редактировать',
                        handler: function (grid, rowIndex, colIndex) {
                            Ext.create('Ext.window.Window', {
                                title: 'Курсы / Редактирование',
                                width: 600,
                                id: 'editCourseWindow',
                                modal: true,
                                layout: 'anchor',
                                items: [
                                    Ext.create('Ext.form.Panel', {
                                        id: 'coursesForm',
                                        bodyStyle: {
                                            padding: '10px 20px'
                                        },
                                        defaults: {
                                            allowBlank: true,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            xtype: 'textfield',
                                            margin: '0 0 10 0',
                                            labelAlign: 'top'

                                        },
                                        url: '/admin/courses/ajax?action=editCourse',
                                        layout: 'anchor',
                                        items: [
                                            {
                                                name: 'id',
                                                hidden: true
                                            },
                                            {
                                                fieldLabel: 'Название *',
                                                name: 'title',
                                                allowBlank: false
                                            },
                                            {
                                                xtype: 'datefield',
                                                allowBlank: false,
                                                format: 'Y-m-d',
                                                fieldLabel: 'Дата начало  *',
                                                name: 'start_date'
                                            },
                                            {
                                                fieldLabel: 'Краткое описание  *',
                                                name: 'sh_content',
                                                allowBlank: false
                                            },
                                            {
                                                xtype: 'textarea',
                                                fieldLabel: 'Описание  *',
                                                name: 'content',
                                                allowBlank: false
                                            },
                                            {
                                                fieldLabel: 'Ссылка',
                                                name: 'link',
                                                allowBlank: true
                                            },
                                            {
                                                fieldLabel: 'Цена *',
                                                name: 'price',
                                                allowBlank: false
                                            },
                                            {
                                                fieldLabel: 'Цена (описание) *',
                                                name: 'price_content'
                                            },
                                            {
                                                xtype: 'checkbox',
                                                fieldLabel: 'Опубликовать*',
                                                name: 'is_active',
                                                labelAlign: 'left',
                                                checked: true,
                                                inputValue: 1,
                                                uncheckedValue: 0
                                            },
                                            {
                                                xtype: 'filefield',
                                                id: 'fileUploadField',
                                                fieldLabel: 'Файл *',
                                                name: 'file'
                                            },
                                            {
                                                xtype: 'box',
                                                margin: '5 5 5 15',
                                                html: Ext.getCmp('coursesGrid').getStore().getAt(rowIndex).data.file
                                            }
                                        ],
                                        buttons: [
                                            {
                                                text: 'Очистить',
                                                handler: function () {
                                                    this.up('form').getForm().reset();
                                                }
                                            },
                                            {
                                                text: 'Сохранить',
                                                formBind: true,
                                                disabled: true,
                                                handler: function () {
                                                    var form = this.up('form').getForm();
                                                    if (form.isValid()) {
                                                        form.submit({
                                                            params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                            success: function (form, action) {
                                                                Ext.getCmp('coursesGrid').getStore().load();
                                                                Ext.getCmp('editCourseWindow').close();
                                                            },
                                                            failure: function (form, action) {
                                                                var res = JSON.parse(action.response.responseText);
                                                                console.log(res);
                                                                if (!res.status) {
                                                                    var msg = '';
                                                                    $.each(res.errors, function (i, val) {
                                                                        var br = '';
                                                                        if (i !== 0) br = '<br>';
                                                                        msg += br + val;
                                                                    });
                                                                    Ext.Msg.alert('Ошибка', msg);
                                                                }

                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        ]
                                    })
                                ]
                            }).show();
                            var rec = Ext.getCmp('coursesGrid').getStore().getAt(rowIndex);
                            var panel = Ext.getCmp('coursesForm');
                            panel.getForm().loadRecord(rec);
                        }
                    },
                    {
                        icon: '/assets/admin/img/delete_task.png',
                        tooltip: 'удалить',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = Ext.getCmp('coursesGrid').getStore().getAt(rowIndex);
                            Ext.Ajax.request({
                                url: '/admin/courses/ajax?action=deleteCourse',
                                method: 'POST',
                                params: {_token: $('meta[name="csrf-token"]').attr('content'), id: rec.data.id},
                                success: function (response) {
                                    var values = Ext.decode(response.responseText);
                                    if (values.success) {
                                        Ext.getCmp('coursesGrid').getStore().load();
                                    }
                                },
                                failure: function (response) {
                                    var res = Ext.decode(response.responseText);
                                    if (!res.status) {
                                        var msg = '';
                                        $.each(res.errors, function (i, val) {
                                            var br = '';
                                            if (i !== 0) br = '<br>';
                                            msg += br + val;
                                        });
                                        Ext.Msg.alert('Ошибка', msg);
                                    }
                                }
                            });
                        }
                    }
                ]
            }
        ],
        listeners: {
            select: function () {
                Ext.getCmp('coursesGrid').getStore().getProxy().extraParams._token = $('meta[name="csrf-token"]').attr('content');
            }
        },
        tbar: [
            {
                xtype: 'textfield',
                width: 350,
                emptyText: 'Найти контента',
                listeners: {
                    change: function (data, record) {
                        var value = record;
                        var grid = Ext.getCmp('coursesGrid'),
                            filter = grid.filters.getFilter('title');

                        if (!filter) {
                            filter = grid.filters.addFilter({
                                active: true,
                                type: 'string',
                                dataIndex: 'title'
                            });

                            filter.menu.show();
                            filter.setValue(value);
                            filter.menu.hide();
                        } else {
                            filter.setValue(value);
                            filter.setActive(true);
                        }
                    }
                }
            },
            '->',
            {
                text: 'Добавить',
                scope: this,
                handler: function () {
                    Ext.create('Ext.window.Window', {
                        title: 'Блоки',
                        id: 'createCourse',
                        width: 600,
                        modal: true,
                        layout: 'anchor',
                        items: [
                            Ext.create('Ext.form.Panel', {
                                id: 'coursesForm',
                                bodyStyle: {
                                    padding: '10px 20px'
                                },
                                defaults: {
                                    allowBlank: true,
                                    msgTarget: 'side',
                                    anchor: '100%',
                                    xtype: 'textfield',
                                    margin: '0 0 10 0',
                                    labelAlign: 'top'

                                },
                                url: '/admin/courses/ajax?action=addCourse',
                                layout: 'anchor',
                                items: [
                                    {
                                        fieldLabel: 'Название *',
                                        name: 'title',
                                        allowBlank: false
                                    },
                                    {
                                        xtype: 'datefield',
                                        format: 'Y-m-d',
                                        allowBlank: false,
                                        fieldLabel: 'Дата начало  *',
                                        name: 'start_date'
                                    },
                                    {
                                        fieldLabel: 'Краткое описание  *',
                                        name: 'sh_content',
                                        allowBlank: false
                                    },
                                    {
                                        xtype: 'textarea',
                                        fieldLabel: 'Описание  *',
                                        name: 'content',
                                        allowBlank: false
                                    },
                                    {
                                        fieldLabel: 'Ссылка',
                                        name: 'link',
                                        allowBlank: false
                                    },
                                    {
                                        fieldLabel: 'Цена *',
                                        name: 'price',
                                        allowBlank: false
                                    },
                                    {
                                        fieldLabel: 'Цена (описание) *',
                                        name: 'price_content'
                                    },
                                    {
                                        xtype: 'checkbox',
                                        fieldLabel: 'Опубликовать*',
                                        name: 'is_active',
                                        labelAlign: 'left',
                                        checked: true,
                                        inputValue: 1,
                                        uncheckedValue: 0
                                    },
                                    {
                                        xtype: 'filefield',
                                        fieldLabel: 'Файл *',
                                        name: 'file'
                                    }
                                ],
                                buttons: [
                                    {
                                        text: 'Очистить',
                                        handler: function () {
                                            this.up('form').getForm().reset();
                                        }
                                    },
                                    {
                                        text: 'Сохранить',
                                        formBind: true,
                                        disabled: true,
                                        handler: function () {
                                            var form = this.up('form').getForm();
                                            if (form.isValid()) {
                                                form.submit({
                                                    params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                    success: function (form, action) {
                                                        Ext.getCmp('coursesForm').getForm().reset();
                                                        Ext.getCmp('coursesGrid').getStore().load();
                                                    },
                                                    failure: function (form, action) {
                                                        var res = JSON.parse(action.response.responseText);
                                                        if (!res.status) {
                                                            var msg = '';
                                                            $.each(res.errors, function (i, val) {
                                                                var br = '';
                                                                if (i !== 0) br = '<br>';
                                                                msg += br + val;
                                                            });
                                                            Ext.Msg.alert('Ошибка', msg);
                                                        }

                                                    }
                                                });
                                            }
                                        }
                                    }
                                ]
                            })
                        ]
                    }).show();
                }
            }
        ],
        bbar: [
            Ext.create('Ext.PagingToolbar', {
                pageSize: 50,
                store: Ext.data.StoreManager.lookup('coursesStore'),
                displayInfo: true
            })
        ],
        plugins: [],
        viewConfig: {
            loadMask: {
                msg: 'загрузка...'
            }
        }
    });
});
Ext.create('Ext.data.JsonStore', {
    storeId: 'coursesStore',
    autoLoad: true,
    fields: ['id', 'title', 'start_date', 'week', 'sh_content', 'content', 'link', 'price', 'price_content', 'is_active', 'file', 'start_date_string'],
    pageSize: 30,
    remoteSort: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/admin/courses/ajax?action=courses'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    },
    sorters: [
        {property: 'id', direction: 'DESC'}
    ]
});


