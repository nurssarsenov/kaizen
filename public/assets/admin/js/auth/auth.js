$.fn.api.settings.api = {
    'authorization': '/auth/ajax'
};

Auth = {
    changePassword: function(){
        console.log(window.location.href);
        $('#resetPasswordForm')
            .form({
                fields: {
                    password: {
                        identifier: 'password',
                        rules: [
                            {
                                type: 'regExp[/[A-za-z]+/ig]',
                                prompt: 'Пароль должен содержать только латиницу'
                            },
                            {
                                type: 'minLength[6]',
                                prompt: 'Пароль должен быть не меньше 6 символов'
                            }
                        ]
                    },
                    password2: {
                        identifier: 'password2',
                        rules: [
                            {
                                type: 'match[password]',
                                prompt: 'Пароли не совпадают',

                            }
                        ]
                    }
                },
                onFailure: function (formErrors, fields) {
                    $.each(formErrors, function (key, value) {
                        console.log(value);
                        toastr.error(value);
                    });
                    console.log(formErrors, fields);
                    return false;
                }
            })
            .api({
                url: '/changePassword',
                method: 'POST',
                urlData: {},
                data: {
                    password: $('#password').val(),
                    password2: $('#password2').val(),
                    url: window.location.href,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                onSuccess: function (response) {
                    if (response.status) {
                        $.each(response.msgs, function (key, value) {
                            toastr.success(value);
                        });
                        setTimeout(function() {
                            window.location.href = response.redirect
                        }, 3000);

                    }
                },
                onFailure: function (response) {
                    if (!response.status) {
                        if (response.errors) {
                            $.each(response.errors, function (key, value) {
                                console.log(value);
                                toastr.error(value);
                            });
                        }
                    }
                }
            })
    },
    reset: function(){
        $('#resetPasswordForm')
            .form({
                fields: {
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'email',
                                prompt: 'Поле Email должно быть действительным электронным адресом.'
                            }
                        ]
                    }
                },
                onFailure: function (formErrors, fields) {
                    $.each(formErrors, function (key, value) {
                        console.log(value);
                        toastr.error(value);
                    });
                    console.log(formErrors, fields);
                    return false;
                }
            })
            .api({
                url: '/resetPassword',
                method: 'POST',
                urlData: {},
                data: {
                    action: "resetPassword",
                    email: $('#authLogin').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                onSuccess: function (response) {
                    console.log("ASASSA");
                    if (response.status) {
                        $.each(response.msgs, function (key, value) {
                            toastr.success(value);
                        });
                        setTimeout(function() {
                            window.location.href = response.redirect
                        }, 3000);

                    }
                },
                onFailure: function (response) {
                    if (!response.status) {
                        if (response.errors) {
                            $.each(response.errors, function (key, value) {
                                console.log(value);
                                toastr.error(value);
                            });
                        }
                    }
                }
            })
    },
    login: function () {
        $('#authForm')
            .form({
                fields: {
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'email',
                                prompt: 'Поле Email должно быть действительным электронным адресом.'
                            }
                        ]
                    },
                    password: {
                        identifier: 'password',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Поле Пароль обязательно для заполнения.'
                            },
                            {
                                type: 'minLength[6]',
                                prompt: 'Поле Пароль должно быть не менее  {ruleValue} символов'
                            }
                        ]
                    }
                },
                onFailure: function (formErrors, fields) {
                    $.each(formErrors, function (key, value) {
                        console.log(value);
                        toastr.error(value);
                    });
                    console.log(formErrors, fields);
                    return false;
                }
            })
            .api({
                action: 'authorization',
                method: 'POST',
                urlData: {},
                data: {
                    action: "authorization",
                    email: $('#authLogin').val(),
                    password: $('#authPassword').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                onSuccess: function (response) {
                    console.log("redirect to " + response.redirect);
                    if (response.status) {
                        window.location.href = response.redirect;
                    }
                },
                onFailure: function (response) {
                    if (!response.status) {
                        if (response.errors) {
                            $.each(response.errors, function (key, value) {
                                console.log("Some Error ");
                                console.log(value);
                                toastr.error(value);
                            });
                        }
                    }
                }
            })
        ;
    }
};

