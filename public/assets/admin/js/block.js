Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '/assets/libs/extjs/ux');
Ext.require(['*', 'Ext.ux.grid.FiltersFeature', 'Ext.window.*', 'Ext.ux.statusbar.StatusBar', 'Ext.selection.CellModel', 'Ext.ux.CheckColumn', 'Ext.tip.*', 'Ext.tree.*']);
Ext.tip.QuickTipManager.init();

Ext.onReady(function () {
    Ext.create('Ext.grid.Panel', {
        id: 'blocksGrid',
        title: 'Список блоков',
        store: Ext.data.StoreManager.lookup('blocksStore'),
        columnLines: true,
        renderTo: 'blocksGrid',
        anchor: '100% 100%',
        height: '80vh',
        features: [{
            ftype: 'filters',
            encode: false,
            local: false,
            filters: []
        }],
        columns: [
            {text: 'ID', width: 60, dataIndex: 'id', filter: {type: 'int'}},
            {text: 'Название', flex: 1, dataIndex: 'title', filter: {type: 'string'}},
            {text: 'Краткое описание', flex: 1, dataIndex: 'sh_content', filter: {type: 'string'}},
            {
                text: 'Состояние', flex: 1, dataIndex: 'is_active', filter: {type: 'int'},
                renderer: function (v) {
                    var state = ['скрытый', 'опубликовано'];
                    return state[v];
                }
            },
            {
                text: 'Тип', flex: 1, dataIndex: 'type', filter: {type: 'int'},
                renderer: function (v) {
                    var types = ["Текст", "Контент"];
                    return types[v];
                }
            },
            {
                xtype: 'actioncolumn',
                items: [
                    {
                        icls: '',
                        icon: '/assets/admin/img/edit_task.png',
                        tooltip: 'редактировать',
                        handler: function (grid, rowIndex, colIndex) {
                            Ext.create('Ext.window.Window', {
                                title: 'Блоки / Редактирование',
                                width: 600,
                                id: 'editBlockWindow',
                                modal: true,
                                layout: 'anchor',
                                items: [
                                    Ext.create('Ext.form.Panel', {
                                        id: 'blocksForm',
                                        bodyStyle: {
                                            padding: '10px 20px'
                                        },
                                        defaults: {
                                            allowBlank: true,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            xtype: 'textfield',
                                            margin: '0 0 10 0',
                                            labelAlign: 'top'

                                        },
                                        url: '/admin/blocks/ajax?action=editBlock',
                                        layout: 'anchor',
                                        items: [
                                            {
                                                name: 'id',
                                                hidden: true
                                            },
                                            {
                                                fieldLabel: 'Название *',
                                                name: 'title',
                                                allowBlank: false
                                            },
                                            {
                                                fieldLabel: 'Краткое описание *',
                                                name: 'sh_content'
                                            },
                                            {
                                                xtype: 'textarea',
                                                fieldLabel: 'Описание *',
                                                name: 'content'
                                            },
                                            {
                                                xtype: 'checkbox',
                                                fieldLabel: 'Опубликовать*',
                                                name: 'is_active',
                                                labelAlign: 'left',
                                                checked: true,
                                                inputValue: 1,
                                                uncheckedValue: 0
                                            },
                                            {
                                                xtype: 'combobox',
                                                fieldLabel: 'Тип *',
                                                name: 'type',
                                                store: type,
                                                valueField: 'id',
                                                editable: false,
                                                displayField: 'type',
                                                queryMode: 'local',
                                                emptyText: 'Выберите тип...',
                                                allowBlank: false,
                                            },
                                            {
                                                xtype: 'filefield',
                                                id: 'fileUploadField',
                                                fieldLabel: 'Файл *',
                                                name: 'file',
                                                editable: false
                                            },
                                            {
                                                xtype: 'box',
                                                margin: '5 5 5 15',
                                                html: Ext.getCmp('blocksGrid').getStore().getAt(rowIndex).data.file
                                            },
                                        ],
                                        buttons: [
                                            {
                                                text: 'Очистить',
                                                handler: function () {
                                                    this.up('form').getForm().reset();
                                                }
                                            },
                                            {
                                                text: 'Сохранить',
                                                formBind: true,
                                                disabled: true,
                                                handler: function () {
                                                    var form = this.up('form').getForm();
                                                    if (form.isValid()) {
                                                        form.submit({
                                                            params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                            success: function (form, action) {
                                                                Ext.getCmp('blocksGrid').getStore().load();
                                                                Ext.getCmp('editBlockWindow').close();
                                                            },
                                                            failure: function (form, action) {
                                                                var res = JSON.parse(action.response.responseText);
                                                                if (!res.status) {
                                                                    var msg = '';
                                                                    $.each(res.errors, function (i, val) {
                                                                        var br = '';
                                                                        if (i !== 0) br = '<br>';
                                                                        msg += br + val;
                                                                    });
                                                                    Ext.Msg.alert('Ошибка', msg);
                                                                }

                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        ]
                                    })
                                ]
                            }).show();
                            var rec = Ext.getCmp('blocksGrid').getStore().getAt(rowIndex);
                            var panel = Ext.getCmp('blocksForm');
                            panel.getForm().loadRecord(rec);
                        }
                    },
                    {
                        icon: '/assets/admin/img/delete_task.png',
                        tooltip: 'удалить',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = Ext.getCmp('blocksGrid').getStore().getAt(rowIndex);
                            Ext.Ajax.request({
                                url: '/admin/blocks/ajax?action=deleteBlock',
                                method: 'POST',
                                params: {_token: $('meta[name="csrf-token"]').attr('content'), id: rec.data.id},
                                success: function (response) {
                                    var values = Ext.decode(response.responseText);
                                    if (values.success) {
                                        Ext.getCmp('blocksGrid').getStore().load();
                                    }
                                },
                                failure: function (response) {
                                    var res = Ext.decode(response.responseText);
                                    if (!res.status) {
                                        var msg = '';
                                        $.each(res.errors, function (i, val) {
                                            var br = '';
                                            if (i !== 0) br = '<br>';
                                            msg += br + val;
                                        });
                                        Ext.Msg.alert('Ошибка', msg);
                                    }
                                }
                            });
                        }
                    }
                ]
            }
        ],
        listeners: {
            select: function () {
                Ext.getCmp('blocksGrid').getStore().getProxy().extraParams._token = $('meta[name="csrf-token"]').attr('content');
            }
        },
        tbar: [
            {
                xtype: 'textfield',
                width: 350,
                emptyText: 'Найти блока',
                listeners: {
                    change: function (data, record) {
                        var value = record;
                        var grid = Ext.getCmp('blocksGrid'),
                            filter = grid.filters.getFilter('title');

                        if (!filter) {
                            filter = grid.filters.addFilter({
                                active: true,
                                type: 'string',
                                dataIndex: 'title'
                            });

                            filter.menu.show();
                            filter.setValue(value);
                            filter.menu.hide();
                        } else {
                            filter.setValue(value);
                            filter.setActive(true);
                        }
                    }
                }
            },
            '->',
            {
                text: 'Добавить',
                scope: this,
                handler: function () {
                    Ext.create('Ext.window.Window', {
                        title: 'Блоки',
                        id: 'createBlock',
                        width: 600,
                        modal: true,
                        layout: 'anchor',
                        items: [
                            Ext.create('Ext.form.Panel', {
                                id: 'blocksForm',
                                bodyStyle: {
                                    padding: '10px 20px'
                                },
                                defaults: {
                                    allowBlank: true,
                                    msgTarget: 'side',
                                    anchor: '100%',
                                    xtype: 'textfield',
                                    margin: '0 0 10 0',
                                    labelAlign: 'top'

                                },
                                url: '/admin/blocks/ajax?action=addBlock',
                                layout: 'anchor',
                                items: [
                                    {
                                        fieldLabel: 'Название *',
                                        name: 'title',
                                        allowBlank: false
                                    },
                                    {
                                        fieldLabel: 'Краткое описание *',
                                        name: 'sh_content'
                                    },
                                    {
                                        xtype: 'textarea',
                                        fieldLabel: 'Описание *',
                                        name: 'content'
                                    },
                                    {
                                        xtype: 'checkbox',
                                        fieldLabel: 'Опубликовать*',
                                        name: 'is_active',
                                        labelAlign: 'left',
                                        checked: true,
                                        inputValue: 1,
                                        uncheckedValue: 0
                                    },
                                    {
                                        xtype: 'combobox',
                                        fieldLabel: 'Тип *',
                                        name: 'type',
                                        store: type,
                                        valueField: 'id',
                                        editable: false,
                                        displayField: 'type',
                                        queryMode: 'local',
                                        emptyText: 'Выберите тип...',
                                        allowBlank: false
                                    },
                                    {
                                        xtype: 'filefield',
                                        fieldLabel: 'Файл *',
                                        name: 'file',
                                        editable: false
                                    }
                                ],
                                buttons: [
                                    {
                                        text: 'Очистить',
                                        handler: function () {
                                            this.up('form').getForm().reset();
                                        }
                                    },
                                    {
                                        text: 'Сохранить',
                                        formBind: true,
                                        disabled: true,
                                        handler: function () {
                                            var form = this.up('form').getForm();
                                            if (form.isValid()) {
                                                form.submit({
                                                    params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                    success: function (form, action) {
                                                        Ext.getCmp('blocksForm').getForm().reset();
                                                        Ext.getCmp('blocksGrid').getStore().load();
                                                    },
                                                    failure: function (form, action) {
                                                        var res = JSON.parse(action.response.responseText);
                                                        if (!res.status) {
                                                            var msg = '';
                                                            $.each(res.errors, function (i, val) {
                                                                var br = '';
                                                                if (i !== 0) br = '<br>';
                                                                msg += br + val;
                                                            });
                                                            Ext.Msg.alert('Ошибка', msg);
                                                        }

                                                    }
                                                });
                                            }
                                        }
                                    }
                                ]
                            })
                        ]
                    }).show();
                }
            }
        ],
        bbar: [
            Ext.create('Ext.PagingToolbar', {
                pageSize: 50,
                store: Ext.data.StoreManager.lookup('blocksStore'),
                displayInfo: true
            })
        ],
        plugins: [],
        viewConfig: {
            loadMask: {
                msg: 'загрузка...'
            }
        }
    });
});
Ext.create('Ext.data.JsonStore', {
    storeId: 'blocksStore',
    autoLoad: true,
    fields: ['id', 'title', 'sh_content', 'content', 'is_active', 'type', 'file'],
    pageSize: 30,
    remoteSort: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/admin/blocks/ajax?action=blocks'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    },
    sorters: [
        {property: 'id', direction: 'DESC'}
    ]
});
var type = Ext.create('Ext.data.Store', {
    fields: ['id', 'type'],
    data: [
        {"id": 0, "type": "Текст"},
        {"id": 1, "type": "Контент"}
    ]
});

