Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '/assets/libs/extjs/ux');
Ext.require(['*', 'Ext.ux.grid.FiltersFeature', 'Ext.window.*', 'Ext.ux.statusbar.StatusBar', 'Ext.selection.CellModel', 'Ext.ux.CheckColumn', 'Ext.tip.*', 'Ext.tree.*',
    'Ext.layout.component.field.HtmlEditor']);
Ext.tip.QuickTipManager.init();

Ext.onReady(function () {
    Ext.create('Ext.grid.Panel', {
        id: 'staticBlocksGrid',
        title: 'Список статических блоков',
        store: Ext.data.StoreManager.lookup('staticBlocksStore'),
        columnLines: true,
        renderTo: 'staticBlocksGrid',
        anchor: '100% 100%',
        height: '80vh',
        features: [{
            ftype: 'filters',
            encode: false,
            local: false,
            filters: []
        }],
        columns: [
            {text: 'ID', width: 60, dataIndex: 'id', filter: {type: 'int'}},
            {text: 'Название', flex: 1, dataIndex: 'title', filter: {type: 'string'}},
            {
                text: 'Тип', flex: 1, dataIndex: 'type', filter: {type: 'int'},
                renderer: function (v) {
                    var state = ["Реквизиты", "Адрес", "Телефон", "Почта", "Геолокация", "Соц. сеть",'Меню (header)','Меню (footer)'];
                    return state[v];
                }
            },
            {
                text: 'Состояние', flex: 1, dataIndex: 'is_active', filter: {type: 'int'},
                renderer: function (v) {
                    var state = ['скрытый', 'опубликовано'];
                    return state[v];
                }
            },
            {
                xtype: 'actioncolumn',
                items: [
                    {
                        icls: '',
                        icon: '/assets/admin/img/edit_task.png',
                        tooltip: 'редактировать',
                        handler: function (grid, rowIndex, colIndex) {
                            Ext.create('Ext.window.Window', {
                                title: 'Статические блоки / Редактирование',
                                width: 600,
                                id: 'editStaticBlockWindow',
                                modal: true,
                                layout: 'anchor',
                                items: [
                                    Ext.create('Ext.form.Panel', {
                                        id: 'staticBlocksForm',
                                        bodyStyle: {
                                            padding: '10px 20px'
                                        },
                                        defaults: {
                                            allowBlank: true,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            xtype: 'textfield',
                                            margin: '0 0 10 0',
                                            labelAlign: 'top'

                                        },
                                        url: '/admin/static-blocks/ajax?action=editStaticBlock',
                                        layout: 'anchor',
                                        items: [
                                            {
                                                name: 'id',
                                                hidden: true
                                            },
                                            {
                                                fieldLabel: 'Название *',
                                                name: 'title',
                                                allowBlank: false
                                            },
                                            {
                                                xtype: 'combobox',
                                                fieldLabel: 'Тип *',
                                                name: 'type',
                                                store: types,
                                                valueField: 'id',
                                                editable: false,
                                                displayField: 'title',
                                                queryMode: 'local',
                                                emptyText: 'Выберите тип...',
                                                allowBlank: false
                                            },
                                            {
                                                xtype: 'htmleditor',
                                                fieldLabel: 'Контент *',
                                                name: 'content',
                                                allowBlank: false
                                            },
                                            {
                                                xtype: 'checkbox',
                                                fieldLabel: 'Опубликовать*',
                                                name: 'is_active',
                                                labelAlign: 'left',
                                                checked: true,
                                                inputValue: 1,
                                                uncheckedValue: 0
                                            }
                                        ],
                                        buttons: [
                                            {
                                                text: 'Очистить',
                                                handler: function () {
                                                    this.up('form').getForm().reset();
                                                }
                                            },
                                            {
                                                text: 'Сохранить',
                                                formBind: true,
                                                disabled: true,
                                                handler: function () {
                                                    var form = this.up('form').getForm();
                                                    if (form.isValid()) {
                                                        form.submit({
                                                            params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                            success: function (form, action) {
                                                                Ext.getCmp('staticBlocksGrid').getStore().load();
                                                                Ext.getCmp('editStaticBlockWindow').close();
                                                            },
                                                            failure: function (form, action) {
                                                                var res = JSON.parse(action.response.responseText);
                                                                console.log(res);
                                                                if (!res.status) {
                                                                    var msg = '';
                                                                    $.each(res.errors, function (i, val) {
                                                                        var br = '';
                                                                        if (i !== 0) br = '<br>';
                                                                        msg += br + val;
                                                                    });
                                                                    Ext.Msg.alert('Ошибка', msg);
                                                                }

                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        ]
                                    })
                                ]
                            }).show();
                            var rec = Ext.getCmp('staticBlocksGrid').getStore().getAt(rowIndex);
                            var panel = Ext.getCmp('staticBlocksForm');
                            panel.getForm().loadRecord(rec);
                        }
                    },
                    {
                        icon: '/assets/admin/img/delete_task.png',
                        tooltip: 'удалить',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = Ext.getCmp('staticBlocksGrid').getStore().getAt(rowIndex);
                            Ext.Ajax.request({
                                url: '/admin/static-blocks/ajax?action=deleteStaticBlock',
                                method: 'POST',
                                params: {_token: $('meta[name="csrf-token"]').attr('content'), id: rec.data.id},
                                success: function (response) {
                                    var values = Ext.decode(response.responseText);
                                    if (values.success) {
                                        Ext.getCmp('staticBlocksGrid').getStore().load();
                                    }
                                },
                                failure: function (response) {
                                    var res = Ext.decode(response.responseText);
                                    if (!res.status) {
                                        var msg = '';
                                        $.each(res.errors, function (i, val) {
                                            var br = '';
                                            if (i !== 0) br = '<br>';
                                            msg += br + val;
                                        });
                                        Ext.Msg.alert('Ошибка', msg);
                                    }
                                }
                            });
                        }
                    }
                ]
            }
        ],
        listeners: {
            select: function () {
                Ext.getCmp('staticBlocksGrid').getStore().getProxy().extraParams._token = $('meta[name="csrf-token"]').attr('content');
            }
        },
        tbar: [
            {
                xtype: 'textfield',
                width: 350,
                emptyText: 'Найти блока',
                listeners: {
                    change: function (data, record) {
                        var value = record;
                        var grid = Ext.getCmp('staticBlocksGrid'),
                            filter = grid.filters.getFilter('title');

                        if (!filter) {
                            filter = grid.filters.addFilter({
                                active: true,
                                type: 'string',
                                dataIndex: 'title'
                            });

                            filter.menu.show();
                            filter.setValue(value);
                            filter.menu.hide();
                        } else {
                            filter.setValue(value);
                            filter.setActive(true);
                        }
                    }
                }
            },
            '->',
            {
                text: 'Добавить',
                scope: this,
                handler: function () {
                    Ext.create('Ext.window.Window', {
                        title: 'Статические блоки',
                        id: 'createStaticBlock',
                        width: 600,
                        modal: true,
                        layout: 'anchor',
                        items: [
                            Ext.create('Ext.form.Panel', {
                                id: 'staticBlocksForm',
                                bodyStyle: {
                                    padding: '10px 20px'
                                },
                                defaults: {
                                    allowBlank: true,
                                    msgTarget: 'side',
                                    anchor: '100%',
                                    xtype: 'textfield',
                                    margin: '0 0 10 0',
                                    labelAlign: 'top'

                                },
                                url: '/admin/static-blocks/ajax?action=addStaticBlock',
                                layout: 'anchor',
                                items: [
                                    {
                                        fieldLabel: 'Название *',
                                        name: 'title',
                                        allowBlank: false
                                    },
                                    {
                                        xtype: 'combobox',
                                        fieldLabel: 'Тип *',
                                        name: 'type',
                                        store: types,
                                        valueField: 'id',
                                        editable: false,
                                        displayField: 'title',
                                        queryMode: 'local',
                                        emptyText: 'Выберите тип...',
                                        allowBlank: false
                                    },
                                    {
                                        xtype: 'htmleditor',
                                        fieldLabel: 'Контент  *',
                                        name: 'content',
                                        allowBlank: false
                                    },
                                    {
                                        xtype: 'checkbox',
                                        fieldLabel: 'Опубликовать*',
                                        name: 'is_active',
                                        labelAlign: 'left',
                                        checked: true,
                                        inputValue: 1,
                                        uncheckedValue: 0
                                    }
                                ],
                                buttons: [
                                    {
                                        text: 'Очистить',
                                        handler: function () {
                                            this.up('form').getForm().reset();
                                        }
                                    },
                                    {
                                        text: 'Сохранить',
                                        formBind: true,
                                        disabled: true,
                                        handler: function () {
                                            var form = this.up('form').getForm();
                                            if (form.isValid()) {
                                                form.submit({
                                                    params: {_token: $('meta[name="csrf-token"]').attr('content')},
                                                    success: function (form, action) {
                                                        Ext.getCmp('staticBlocksForm').getForm().reset();
                                                        Ext.getCmp('staticBlocksGrid').getStore().load();
                                                    },
                                                    failure: function (form, action) {
                                                        var res = JSON.parse(action.response.responseText);
                                                        if (!res.status) {
                                                            var msg = '';
                                                            $.each(res.errors, function (i, val) {
                                                                var br = '';
                                                                if (i !== 0) br = '<br>';
                                                                msg += br + val;
                                                            });
                                                            Ext.Msg.alert('Ошибка', msg);
                                                        }

                                                    }
                                                });
                                            }
                                        }
                                    }
                                ]
                            })
                        ]
                    }).show();
                }
            }
        ],
        bbar: [
            Ext.create('Ext.PagingToolbar', {
                pageSize: 50,
                store: Ext.data.StoreManager.lookup('staticBlocksStore'),
                displayInfo: true
            })
        ],
        plugins: [],
        viewConfig: {
            loadMask: {
                msg: 'загрузка...'
            }
        }
    });
});
Ext.create('Ext.data.JsonStore', {
    storeId: 'staticBlocksStore',
    autoLoad: true,
    fields: ['id', 'title', 'content', 'type', 'is_active'],
    pageSize: 30,
    remoteSort: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/admin/static-blocks/ajax?action=staticBlocks'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    },
    sorters: [
        {property: 'id', direction: 'DESC'}
    ]
});
var types = Ext.create('Ext.data.Store', {
    fields: ['id', 'title'],
    data: [
        {"id": 0, "title": "Реквизиты"},
        {"id": 1, "title": "Адрес"},
        {"id": 2, "title": "Телефон"},
        {"id": 3, "title": "Почта"},
        {"id": 4, "title": "Геолокация"},
        {"id": 5, "title": "Меню (header)"},
        {"id": 6, "title": "Меню (footer)"}
    ]
});


