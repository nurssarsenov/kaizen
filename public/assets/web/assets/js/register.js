$.fn.form.settings.rules.customRule = function (param) {
    var split = param.split(' ');
    if (split.length > 1) return true;
    return false;
};
Register = {
    userRegister: function(){
        $('#userRegisterForm')
            .form({
                fields: {
                    fio: {
                        identifier: 'fio',
                        rules: [
                            {
                                type: 'regExp[/[а-я]+/ig]',
                                prompt: 'введите корректный ФИО'
                            },
                            {
                                type: 'customRule[param]',
                                prompt: 'введите полный ФИО'
                            }
                        ]
                    },
                    position: {
                        identifier: 'position',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'введите корректный должность'
                            }
                        ]
                    },
                    company: {
                        identifier: 'company',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'поле компания обязательно для заполнения.'
                            }
                        ]
                    },
                    city: {
                        identifier: 'city',
                        rules: [
                            {
                                type: 'regExp[/[а-я]+/ig]',
                                prompt: 'введите корректный город'
                            }
                        ]
                    },
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'email',
                                prompt: 'поле email должно быть действительным электронным адресом.'
                            }
                        ]
                    },
                    phone: {
                        identifier: 'phone',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'поле телефон должно быть действительным номером.'
                            }
                        ]
                    },
                    password: {
                        identifier: 'password',
                        rules: [
                            {
                                type: 'regExp[/[A-za-z]+/ig]',
                                prompt: 'Пароль должен содержать только латиницу'
                            },
                            {
                                type: 'minLength[6]',
                                prompt: 'Пароль должен быть не меньше 6 символов'
                            }
                        ]
                    },
                    password2: {
                        identifier: 'password2',
                        rules: [
                            {
                                type: 'match[password]',
                                prompt: 'Пароли не совпадают',

                            }
                        ]
                    }
                },
                onFailure: function (formErrors, fields) {
                    $.each(formErrors, function (key, value) {
                        toastr.error(value);
                    });
                    return false;
                }
            })
            .api({
                url: '/register',
                method: 'POST',
                urlData: {},
                data: {
                    fio: $('#fio').val(),
                    position: $('#position').val(),
                    company: $('#company').val(),
                    city: $('#city').val(),
                    email: $('#email').val(),
                    phone: $('#phone').val(),
                    password: $('#password').val(),
                    password2: $('#password2').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                onSuccess: function (response) {
                    if (response.status) {
                        console.log("adasd");
                        if (response.status) {
                            $.each(response.msgs, function (key, value) {
                                toastr.success(value);
                            });
                            setTimeout(function() {
                                window.location.href = response.redirect
                            }, 5000);

                        }
                    }
                },
                onFailure: function (response) {
                    console.log("adasd22323233");
                    if (!response.status) {
                        if (response.errors) {
                            $.each(response.errors, function (key, value) {
                                toastr.error(value);
                            });
                        }
                    }
                }
            });
    },
    courseRegister: function () {
        $('#registerForm')
            .form({
                fields: {
                    fio: {
                        identifier: 'fio',
                        rules: [
                            {
                                type: 'regExp[/[а-я]+/ig]',
                                prompt: 'введите корректный ФИО'
                            },
                            {
                                type: 'customRule[param]',
                                prompt: 'введите полный ФИО'
                            }
                        ]
                    },
                    position: {
                        identifier: 'position',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'введите корректный должность'
                            }
                        ]
                    },
                    company: {
                        identifier: 'company',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'поле компания обязательно для заполнения.'
                            }
                        ]
                    },
                    city: {
                        identifier: 'city',
                        rules: [
                            {
                                type: 'regExp[/[а-я]+/ig]',
                                prompt: 'введите корректный город'
                            }
                        ]
                    },
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'email',
                                prompt: 'поле email должно быть действительным электронным адресом.'
                            }
                        ]
                    },
                    phone: {
                        identifier: 'phone',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'поле телефон должно быть действительным номером.'
                            }
                        ]
                    }
                },
                onFailure: function (formErrors, fields) {
                    $.each(formErrors, function (key, value) {
                        toastr.error(value);
                    });
                    return false;
                }
            })
            .api({
                url: '/registration-course',
                method: 'POST',
                urlData: {},
                data: {
                    fio: $('#fio').val(),
                    position: $('#position').val(),
                    company: $('#company').val(),
                    city: $('#city').val(),
                    email: $('#email').val(),
                    phone: $('#phone').val(),
                    course: $('#course').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                onSuccess: function (response) {
                    if (response.status) {
                        window.location.href = response.redirect;
                    }
                },
                onFailure: function (response) {
                    if (!response.status) {
                        if (response.errors) {
                            $.each(response.errors, function (key, value) {
                                toastr.error(value);
                            });
                        }
                    }
                }
            });
    },
    memberUpdate: function (btn) {
        var closest = btn.closest(".memberUpdate");
        $(closest)
            .form({
                fields: {
                    fio: {
                        identifier: 'fio',
                        rules: [
                            {
                                type: 'regExp[/[а-я]+/ig]',
                                prompt: 'введите корректный ФИО'
                            },
                            {
                                type: 'customRule[param]',
                                prompt: 'введите полный ФИО'
                            }
                        ]
                    },
                    position: {
                        identifier: 'position',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'введите корректный должность'
                            }
                        ]
                    },
                    company: {
                        identifier: 'company',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'поле компания обязательно для заполнения.'
                            }
                        ]
                    },
                    city: {
                        identifier: 'city',
                        rules: [
                            {
                                type: 'regExp[/[а-я]+/ig]',
                                prompt: 'введите корректный город'
                            }
                        ]
                    },
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'email',
                                prompt: 'поле email должно быть действительным электронным адресом.'
                            }
                        ]
                    },
                    phone: {
                        identifier: 'phone',
                        rules: [
                            {
                                type: 'regExp[/[+7]+[ ]+[(]+[7]+[0-9]{2}[)]+[ ]+[0-9]{3}[-][0-9]{2}[-]+[0-9]{2}/ig]',
                                prompt: 'поле телефон должно быть действительным номером.'
                            }
                        ]
                    }
                },
                onFailure: function (formErrors, fields) {
                    $.each(formErrors, function (key, value) {
                        toastr.error(value);
                    });
                    return false;
                }
            })
            .api({
                url: '/payments/ajax',
                method: 'POST',
                urlData: {},
                data: {
                    action: 'memberUpdate',
                    user_id: closest.find('.userId').val(),
                    fio: closest.find('.fio').val(),
                    position: closest.find('.position').val(),
                    company: closest.find('.company').val(),
                    city: closest.find('.city').val(),
                    email: closest.find('.email').val(),
                    phone: closest.find('.phone').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                onSuccess: function (response) {
                    if (response.status) {
                        toastr.success(response.message);
                    }
                },
                onFailure: function (response) {
                    if (!response.status) {
                        if (response.errors) {
                            $.each(response.errors, function (key, value) {
                                toastr.error(value);
                            });
                        }
                    }
                }
            });

    },
    memberRemove: function (btn) {
        var closest = btn.closest(".content_members");
        $.ajax({
            url: '/payments/ajax',
            method: 'POST',
            data: {
                action: 'memberDelete',
                user_id: closest.find('.userId').val(),
                token: closest.find('.token').val(),
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.status) {
                    closest.remove();
                    toastr.success(response.message);
                }
            },
            errors: function (response) {
                if (!response.status) {
                    if (response.errors) {
                        $.each(response.errors, function (key, value) {
                            toastr.error(value);
                        });
                    }
                }
            }
        });
    },
    addMember: function (btn) {
        var closest = btn.closest(".members_padded");
        var fio = closest.find('.fio');
        var position = closest.find('.position');
        var company = closest.find('.company');
        var city = closest.find('.city');
        var email = closest.find('.email');
        var phone = closest.find('.phone');
        $(closest)
            .form({
                fields: {
                    fio: {
                        identifier: 'fio',
                        rules: [
                            {
                                type: 'regExp[/[а-я]+/ig]',
                                prompt: 'введите корректный ФИО'
                            },
                            {
                                type: 'customRule[param]',
                                prompt: 'введите полный ФИО'
                            }
                        ]
                    },
                    position: {
                        identifier: 'position',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'введите корректный должность'
                            }
                        ]
                    },
                    company: {
                        identifier: 'company',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'поле компания обязательно для заполнения.'
                            }
                        ]
                    },
                    city: {
                        identifier: 'city',
                        rules: [
                            {
                                type: 'regExp[/[а-я]+/ig]',
                                prompt: 'введите корректный город'
                            }
                        ]
                    },
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'email',
                                prompt: 'поле email должно быть действительным электронным адресом.'
                            }
                        ]
                    },
                    phone: {
                        identifier: 'phone',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'поле телефон должно быть действительным номером.'
                            }
                        ]
                    }
                },
                onFailure: function (formErrors, fields) {
                    $.each(formErrors, function (key, value) {
                        toastr.error(value);
                    });
                    return false;
                }
            })
            .api({
                url: '/payments/ajax',
                method: 'POST',
                urlData: {},
                data: {
                    action: 'addMember',
                    fio: fio.val(),
                    position: position.val(),
                    company: company.val(),
                    city: city.val(),
                    email: email.val(),
                    course: closest.find('.course').val(),
                    phone: phone.val(),
                    token: closest.find('.token').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                onSuccess: function (response) {
                    if (response.status) {
                        $('.members').append(response.html);
                        $('.ui.accordion')
                            .accordion()
                        ;
                        fio.val('');
                        position.val('');
                        company.val('');
                        city.val('');
                        email.val('');
                        phone.val('');
                        toastr.success(response.message);
                        if (response.token) {
                            $('.token').val(response.token);
                        }
                    }
                },
                onFailure: function (response) {
                    if (!response.status) {
                        if (response.errors) {
                            $.each(response.errors, function (key, value) {
                                toastr.error(value);
                            });
                        }
                    }
                }
            });
    },
    downloadPdf: function () {
        $('#entityForm')
            .form({
                fields: {
                    company: {
                        identifier: 'company',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'поле компания обязательно для заполнения.'
                            }
                        ]
                    },
                    bin_iin: {
                        identifier: 'bin_iin',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'поле БИН / ИИН обязательно для заполнения.'
                            }
                        ]
                    },
                    email: {
                        identifier: 'address',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'поле адрес обязательно для заполнения.'
                            }
                        ]
                    }
                },
                onFailure: function (formErrors, fields) {
                    $.each(formErrors, function (key, value) {
                        toastr.error(value);
                    });
                    return false;
                }
            })
            .api({
                url: '/pay/entity/ajax',
                method: 'POST',
                urlData: {},
                data: {
                    bin_iin: $('#bin_iin').val(),
                    company: $('#company').val(),
                    address: $('#address').val(),
                    action: "downloadPdf",
                    payments_token: $('#token').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                onSuccess: function (response) {
                    if (response.status) {
                        window.location.href = response.redirect;
                        toastr.success('Счет фактура успешно сгенерирован');
                        $('#entityForm').find("input[type=text]").val("");
                    }
                },
                onFailure: function (response) {
                    if (!response.status) {
                        if (response.errors) {
                            $.each(response.errors, function (key, value) {
                                toastr.error(value);
                            });
                        }
                    }
                }
            });
    }
};
