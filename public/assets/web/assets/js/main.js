$('header').on('click', '.toggle-navbar', function () {
    var toggle = $(this).data('toggle');
    if (toggle == 0) {
        $(this).addClass('open');
        $(this).data('toggle', 1);
        $('.mobile_header').addClass('menu_show').removeClass('menu_hide');

    } else {
        $(this).removeClass('open');
        $(this).data('toggle', 0);
        $('.mobile_header').addClass('menu_hide').removeClass('menu_show');
    }
});
$('.ui.checkbox').checkbox();
var owl = $('.owl-carousel');
owl.owlCarousel({
    loop: true,
    margin: 0,
    navSpeed: 500,
    nav: true,
    items: 1,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    navigation: true,
    navigationText: [
        "<i class='icon-chevron-left icon-white'></i>",
        "<i class='icon-chevron-right icon-white'></i>"
    ]
});
if ($.inputmask) {
    // $("#phone").inputmask("");
    // $(".phone").inputmask("");
}
$('.delete-user').on('click', function () {
    var user_id = $(this).attr('data-id');
    var payment = $(this).attr('data-payment');
    if (user_id && payment) {
        $.post('/payments/ajax', {
            action: 'paymentDeleteUser',
            user_id: user_id,
            payment_id: payment,
            _token: $('meta[name="csrf-token"]').attr('content')
        }, function (response) {
            if (response.status) {
                $('#' + user_id).remove();
                $('.section_registration__list__all').html(response.html);
                toastr.success(response.message);
            } else {
                toastr.error(response.message);
            }
        });
    }
});
$('.ui.accordion').accordion();
$('.section_file__download___btn').on('click', function () {
    $('.modal-download-pdf').prop('class', $('.modal-download-pdf').prop('class') + " active");
    $('.modal-download-pdf').attr('data-file', $(this).attr('data-file'));
});
$('.section_file__download___btn_2').on('click', function () {
    $('.modal-download-pdf').prop('class', $('.modal-download-pdf').prop('class') + " active");
    $('.modal-download-pdf').attr('data-file', $(this).attr('data-file'));
});
Send = {
    sendEmail: function (object) {
        let input = object.closest('.modal-download-pdf').find('input')[0];
        if (validateEmail(input.value)) {
            $.post("/temp/ajax", {
                    email: input.value, _token: $('meta[name="csrf-token"]').attr('content'), action: 'saveEmail'
                },
                function (responase) {
                    if (responase.status) {
                        toastr.success(responase.message);
                        $('.modal-download-pdf').modal('hide');
                        input.value = '';
                        var file_path = '/uploads/' + object.closest('.modal-download-pdf').attr('data-file');
                        var a = document.createElement('A');
                        a.href = file_path;
                        a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);

                    } else {
                        $.each(response.errors, function (key, value) {
                            toastr.error(value);
                        });
                    }
                });
        } else {
            toastr.error('Введите корректный email адрес');
        }
    }
}
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
