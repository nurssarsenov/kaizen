<?php

namespace App\Providers;

use App\Models\StaticBlock;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $menu_header = StaticBlock::where('id', 8)->first();
        $menu_footer = StaticBlock::where('id', 9)->first();
        $social = StaticBlock::where('id', 7)->first();
        View::share('menu_header', $menu_header);
        View::share('menu_footer', $menu_footer);
        View::share('social', $social);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
