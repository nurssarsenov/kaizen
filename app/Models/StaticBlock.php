<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticBlock extends Model
{
    protected $table = 'static_blocks';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
