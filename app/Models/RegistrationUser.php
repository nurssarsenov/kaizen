<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegistrationUser extends Model
{
    protected $table = 'registration_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
