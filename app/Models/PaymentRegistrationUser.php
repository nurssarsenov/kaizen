<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentRegistrationUser extends Model
{
    protected $table = 'payment_registration_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['payment_id', 'registration_user_id'];
}
