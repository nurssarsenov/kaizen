<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Extjs extends Model
{
    protected $aliases = [];

    public static function jsQuery($query, $limit = 50)
    {
        $request = request();

        //Устанавливаем фильтры
        $filters = $request->get('filter', []);
        if (is_array($filters)) self::setFilters($query, $filters);

        //TOTAL
        $totalQuery = clone $query;
        $totalQuery->orders = [];
        $totalQuery->groups = NULL;
        $totalQuery->select(DB::raw("COUNT(*) total"));
        $totalResult = $totalQuery->first();
        $data['total'] = $totalResult ? (int)$totalResult->total : 0;

        //Лимит
        $limit = (int)$request->get('limit', $limit);
        $offset = (int)$request->get('start', 0);
        $query->offset($offset)->limit($limit);

        //Сортировка
        $sort = $request->get('sort');
        if (!empty($sort)) {
            $sort = json_decode($sort, TRUE);
            self::setSorters($query, $sort);
        }

        $data['data'] = $query->get()->toArray();
        return $data;
    }

    /**
     * Добавление фильтров
     * @param query
     * @param array
     */
    public static function setFilters(&$query, $filters = [])
    {
        //Filtering
        if (empty($filters) OR !is_array($filters)) return;

        $aliases = self::getSqlAliases($query);

        //Добавление фильтров
        foreach ($filters AS $filter) {
            if (!isset($filter['field']) OR !isset($filter['data']['value'])) continue;

            $field = $filter['field'];
            $operator = 'LIKE';
            $value = $filter['data']['value'];
            $type = isset($filter['data']['type']) ? $filter['data']['type'] : "string";

            //заменяем алиас
            if (!empty($aliases)) {
                if (array_key_exists($filter['field'], $aliases))
                    $field = $aliases[$field];
            }

            //Исправляем формат даты
            if ($type == 'date' AND !preg_match("/^\d{4}-\d{2}-\d{2}$/i", $value)) {
                $value = date("Y-m-d", strtotime($value));
            }

            //дата и цифры
            if ($type == 'date' OR $type == 'numeric') {
                $comparison = isset($filter['data']['comparison']) ? $filter['data']['comparison'] : 'eq';
                if ($comparison == 'lt') $operator = "<=";
                else if ($comparison == 'gt') $operator = ">=";
                else $operator = "=";
            }

            if ($type == 'list') {
                $value = explode(",", $value);
                $query->whereIn($field, $value);
                return;
            }

            //Для строковых полей
            if ($operator == 'LIKE') $value = "%" . $value . "%";
            $query->where($field, $operator, $value);
        }
    }

    /**
     * Добавление сортировок
     * @param query
     * @param array
     */
    public static function setSorters(&$query, $sorters = [])
    {
        if (empty($sorters) OR !is_array($sorters)) return;

        $aliases = self::getSqlAliases($query);

        foreach ($sorters AS $sort) {
            if (!isset($sort['property'])) continue;
            if (empty($sort['direction'])) $sort['direction'] = NULL;

            //заменяем алиас
            if (!empty($aliases)) {
                if (array_key_exists($sort['property'], $aliases)) $sort['property'] = $aliases[$sort['property']];
                else continue;
            }

            $query->orderBy($sort['property'], $sort['direction']);
        }
    }

    /**
     * Находим алиасы и имена полей
     * [alias => filed]
     * @param query
     * @param array
     */
    public static function getSqlAliases($query)
    {
        $sql = $query->toSql();

        //Берем часть запроса начиная с SELECT по FROM
        preg_match("/^(\s*select\s+(top\s+\d+\s+)?).+\s+from/is", $sql, $matches);
        if (empty($matches)) return [];

        $sql = reset($matches);
        $sql = preg_replace("/^(\s*select\s+(top\s+\d+\s+)?){1}/i", "", $sql);
        $fields = preg_replace("/\s+from\s*$/i", "", $sql);

        //Берем поля с алиасами
        $aliases = [];
        $pieces = preg_split("/\s*,\s+/", $fields);
        foreach ($pieces AS $value) {
            if (preg_match("/(?<field>.+)\s+(as\s+)?(?<alias>[\w_]+)$/i", $value, $matches)) {
                $alias = $matches;
                //Если в есть функции, то делаем raw
                if (preg_match("/\s*.+\(.+\)\s*/i", $matches['field'])) $matches['field'] = DB::raw($matches['field']);
                $aliases[$matches['alias']] = $matches['field'];
            } else if (preg_match("/(?<prefix>[\w_]+)\.(?<field>[\w_]+)$/i", $value, $matches)) {
                $aliases[$matches['field']] = $matches[0];
            } else $aliases[$value] = $value;
        }
        return $aliases;
    }

    /**
     * Сохраняем данные с ExtJs roweditor
     * @param query
     * @param array
     */
    public static function renderRecordData()
    {
        $values = json_decode(request()->data, true);
        $values = reset($values);
        return $values;
    }

    /**
     * Сохраняем данные с ExtJs roweditor
     * @param query
     * @param array
     */
    public static function saveRecord($model, $fields)
    {
        $values = json_decode(request()->data, true);
        $values = reset($values);

        foreach ($fields AS $field) {
            if (!isset($values[$field])) continue;
            $model->$field = $values[$field];
        }
        return $model->save();
    }
}
