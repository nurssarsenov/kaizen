<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Block;
use App\Models\Content;
use App\Models\Extjs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{
    public function index()
    {
	
        return view('admin.panel.contents.content');
    }

    public function ajax(Request $request)
    {
        $action = $request['action'];

        switch ($action) {
            case "contents":
                $query = Content::join('blocks as b', 'contents.block_id', '=', 'b.id')->select(DB::raw("contents.id, contents.title, contents.sh_content, contents.content, contents.img, contents.is_active, b.title block, contents.block_id, contents.pdf_file"));
                $data = Extjs::jsQuery($query);
                foreach ($data['data'] as $i => $datum) {
                    if ($datum['img']) {
                        $data['data'][$i]['img'] = "<div class='image_preview'><img src='/uploads/" . $datum['img'] . "' alt=''></div>";
                    }
                    if ($datum['pdf_file']) {
                        $data['data'][$i]['pdf_file'] = "<a href='/uploads/" . $datum['pdf_file'] . "' alt='' target='_blank'>" . $datum['pdf_file'] . "</a>";
                    }
                }
                return response()->json($data, 200);
                break;

            case "getBlocks":
                $query = Block::where('type', 1)->select(DB::raw("id, title"));
                $data = Extjs::jsQuery($query);
                return response()->json($data, 200);
                break;

            case "addContent":
                $validator = Validator::make($request->all(), [
                    'block_id' => 'required',
                    'title' => 'required',
                    'img' => 'file',
                ]);

                if ($validator->fails()) {
                    $results['status'] = false;
                    $results['errors'] = $validator->errors()->all();
                    return response()->json($results, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                try {
                    $inputs = $request->except('_token', 'action');
                    if (isset($inputs['img'])) {
                        $filename = str_random(5) . date('Ymd-his') . '.' . $request['img']->getClientOriginalExtension();
                        Storage::disk('uploads')->put($filename, file_get_contents($request['img']->getRealPath()));
                        $inputs['img'] = $filename;
                    }
                    if (isset($inputs['pdf_file'])) {
                        $filename = str_random(5) . date('Ymd-his') . '.' . $request['pdf_file']->getClientOriginalExtension();
                        Storage::disk('uploads')->put($filename, file_get_contents($request['pdf_file']->getRealPath()));
                        $inputs['pdf_file'] = $filename;
                    }
                    Content::create($inputs);
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }

                return response()->json($data, 200);
                break;

            case "editContent":
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'title' => 'required',
                    'block_id' => 'required',
                    'file' => 'file',
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                try {
                    $inputs = $request->except('_token', 'action');
                    $content = Content::find($inputs['id']);
                    if (isset($inputs['img'])) {
                        Storage::disk('uploads')->delete($content->img);
                        $filename = str_random(5) . date('Ymd-his') . '.' . $request['img']->getClientOriginalExtension();
                        Storage::disk('uploads')->put($filename, file_get_contents($request['img']->getRealPath()));
                        $inputs['img'] = $filename;
                    }
                    if (isset($inputs['pdf_file'])) {
                        Storage::disk('uploads')->delete($content->pdf_file);
                        $filename = str_random(5) . date('Ymd-his') . '.' . $request['pdf_file']->getClientOriginalExtension();
                        Storage::disk('uploads')->put($filename, file_get_contents($request['pdf_file']->getRealPath()));
                        $inputs['pdf_file'] = $filename;
                    }
                    Content::where('id', $request['id'])->update($inputs);
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }

                return response()->json($data, 200);
                break;

            case "deleteContent":
                $validator = Validator::make($request->all(), [
                    'id' => 'required'
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => 'Запись успешно удалена');
                try {
                    $content = Content::find($request['id']);
                    Storage::disk('uploads')->delete($content->img);
                    Storage::disk('uploads')->delete($content->pdf_file);
                    $content->delete();
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }
                return response()->json($data, 200);
                break;


            default:
                break;
        }
    }
}
