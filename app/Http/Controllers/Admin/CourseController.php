<?php

namespace App\Http\Controllers\Admin;

use App\Models\Course;
use App\Models\Extjs;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{

    public function getWeek($week)
    {
        $arrs = ["понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресенье"];
        return $arrs[$week];
    }

    public function index()
    {
		
        return view('admin.panel.courses.course');
    }

    public function ajax(Request $request)
    {
        $action = $request['action'];

        switch ($action) {
            case "courses":
                $query = Course::select(DB::raw("id, title, start_date, sh_content, content, link, price, price_content, is_active"));
                $data = Extjs::jsQuery($query);
                $weeks = [1 => "понедельник", 2 => "вторник", 3 => "среда", 4 => "четверг", 5 => "пятница", 6 => "суббота", 7 => "воскресенье"];
                $months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
                if (count($data['data'])) {
                    foreach ($data['data'] as $i => $section_courses_item) {
                        $data['data'][$i]['week'] = $weeks[date('N', strtotime($section_courses_item['start_date']))];
                        $data['data'][$i]['start_date_string'] = date('d', strtotime($section_courses_item['start_date'])) . ' ' . $months[date('m', strtotime($section_courses_item['start_date'])) - 1];
                    }
                }
                return response()->json($data, 200);
                break;

            case "addCourse":
                $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'start_date' => 'required',
                    'sh_content' => 'required',
                    'content' => 'required',
                    'price' => 'required',
                ]);

                if ($validator->fails()) {
                    $results['status'] = false;
                    $results['errors'] = $validator->errors()->all();
                    return response()->json($results, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                try {
                    $inputs = $request->except('_token', 'action');
                    if (isset($inputs['file'])) {
                        $filename = str_random(5) . date('Ymd-his') . '.' . $request['file']->getClientOriginalExtension();
                        Storage::disk('uploads')->put($filename, file_get_contents($request['file']->getRealPath()));
                        $inputs['file'] = $filename;
                    }
                    Course::create($inputs);
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }
                return response()->json($data, 200);
                break;

            case "editCourse":

                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'title' => 'required',
                    'start_date' => 'required',
                    'sh_content' => 'required',
                    'content' => 'required',
                    'price' => 'required',
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                try {
                    $inputs = $request->except('_token', 'action');
                    $course = Course::find($request['id']);
                    if (isset($inputs['file'])) {
                        Storage::disk('uploads')->delete($course->file);
                        $filename = str_random(5) . date('Ymd-his') . '.' . $request['file']->getClientOriginalExtension();
                        Storage::disk('uploads')->put($filename, file_get_contents($request['file']->getRealPath()));
                        $inputs['file'] = $filename;
                    }
                    Course::where('id', $request['id'])->update($inputs);
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }

                return response()->json($data, 200);
                break;

            case "deleteCourse":
                $validator = Validator::make($request->all(), [
                    'id' => 'required'
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => 'Запись успешно удалена');
                try {
                    $course = Course::find($request['id']);
                    Storage::disk('uploads')->delete($course->file);
                    $course->delete();
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }
                return response()->json($data, 200);
                break;


            default:
                break;
        }
    }
}
