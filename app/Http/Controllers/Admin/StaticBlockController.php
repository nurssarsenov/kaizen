<?php

namespace App\Http\Controllers\Admin;

use App\Models\Extjs;
use App\Models\StaticBlock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StaticBlockController extends Controller
{
     public function index()
    {
		
        return view('admin.panel.static_blocks.static_block');
    }

    public function ajax(Request $request)
    {
        $action = $request['action'];

        switch ($action) {
            case "staticBlocks":
                $query = StaticBlock::select(DB::raw("id, title,type, content, is_active"));
                $data = Extjs::jsQuery($query);
                return response()->json($data, 200);
                break;

            case "addStaticBlock":
                $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'type' => 'required',
                    'content' => 'required'
                ]);

                if ($validator->fails()) {
                    $results['status'] = false;
                    $results['errors'] = $validator->errors()->all();
                    return response()->json($results, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                try {
                    $inputs = $request->except('_token', 'action');
                    StaticBlock::create($inputs);
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }
                return response()->json($data, 200);
                break;

            case "editStaticBlock":

                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'title' => 'required',
                    'content' => 'required',
                    'type' => 'required',
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                try {
                    $inputs = $request->except('_token', 'action');
                    StaticBlock::where('id', $request['id'])->update($inputs);
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }

                return response()->json($data, 200);
                break;

            case "deleteStaticBlock":
                $validator = Validator::make($request->all(), [
                    'id' => 'required'
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => 'Запись успешно удалена');
                try {
                    StaticBlock::find($request['id'])->delete();
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }
                return response()->json($data, 200);
                break;


            default:
                break;
        }
    }
}
