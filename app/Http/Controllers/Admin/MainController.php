<?php

namespace App\Http\Controllers\Admin;

use App\Models\Payment;
use App\Models\PaymentRegistrationUser;
use App\Models\RegistrationUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Extjs;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class MainController extends Controller
{
    public function index()
    {
        return view('admin.panel.main');
    }

    public function ajax(Request $request)
    {
        $action = $request['action'];

        switch ($action) {

            case "editOrder":

                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'is_came' => 'required',
                    'is_paid' => 'required'
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                try {
                    $inputs = $request->except('id', '_token', 'fio', 'phone', 'price', 'action');
                    Payment::where('id', PaymentRegistrationUser::where('registration_user_id', $request['id'])->get()->first()->payment_id)->update($inputs);

                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }

                return response()->json($data, 200);
                break;

            case "orders":
                $query = RegistrationUser::join('payment_registration_users as pru', 'registration_users.id', 'pru.registration_user_id')
                    ->join('payments as p', 'pru.payment_id', '=', 'p.id')
                    ->join('courses as c', 'p.course_id', '=', 'c.id')
                    ->leftJoin('entities as e', 'p.token', '=', 'e.payments_token')
                    //->where('p.is_paid', 1)
                    ->select(DB::raw("e.id as entities_id, registration_users.is_company_user, registration_users.created_at, registration_users.position, registration_users.id, registration_users.fio, registration_users.email, registration_users.phone, registration_users.city, registration_users.company,p.is_paid, c.title  course_title, c.price, c.start_date, p.is_came"));

                $data = Extjs::jsQuery($query);
                return response()->json($data, 200);
                break;

            case "delete_order":
                $validator = Validator::make($request->all(), [
                    'id' => 'required'
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => 'Запись успешно удалена');
                try {
                    $total_users = Payment::join('payment_registration_users as ru', 'payments.id', '=', 'ru.payment_id')
                        ->where('ru.registration_user_id', $request['id'])
                        ->select('payments.total_users', 'payments.id', 'payments.token')->first();
                    PaymentRegistrationUser::where('payment_id', $total_users->id)->where('registration_user_id', $request['id'])->delete();
                    RegistrationUser::where('id', $request['id'])->delete();

                    if ($total_users->total_users == 1) {
                        Payment::where('id', $total_users->id)->delete();
                    } else {
                        Payment::where('id', $total_users->id)->decrement('total_users');
                    }

                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }
                return response()->json($data, 200);
                break;
            default:
                return 0;
        }

    }

    public function exportExcel(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        $data = RegistrationUser::join('payment_registration_users as pru', 'registration_users.id', 'pru.registration_user_id')
            ->join('payments as p', 'pru.payment_id', '=', 'p.id')
            ->join('courses as c', 'p.course_id', '=', 'c.id')
            //->where('p.is_paid', 1)
            ->whereBetween('c.start_date', [$request['start_date'], $request['end_date']])
            ->select(DB::raw("registration_users.id, registration_users.fio, registration_users.email, registration_users.phone, registration_users.company, registration_users.city, registration_users.position, c.title, c.price, registration_users.created_at, c.start_date, p.is_paid"))
            ->get();
        Excel::create('orders', function ($excel) use ($data) {
            $excel->sheet('New sheet', function ($sheet) use ($data) {
                $sheet->loadView('web.excel', ['data' => $data]);
            });
        })->download('xlsx');
    }

    public function exportClientExcel(Request $request)
    {
        $row = DB::select('select id,email from emails');
		$data = json_decode(json_encode($row), True);
		
        Excel::create('clients', function ($excel) use ($data) {
            $excel->sheet('New sheet', function ($sheet) use ($data) {
                $sheet->loadView('web.excel-client', ['data' => $data]);
            });
        })->download('xlsx');
    }
}
