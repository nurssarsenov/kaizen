<?php

namespace App\Http\Controllers\Admin;

use App\Models\Block;
use App\Models\Extjs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BlockController extends Controller
{
    public function index()
    {
        return view('admin.panel.blocks.block');
    }

    public function ajax(Request $request)
    {
        $action = $request['action'];

        switch ($action) {
            case "blocks":
                $query = Block::select(DB::raw("id, title, sh_content, content, is_active, type, file"));
                $data = Extjs::jsQuery($query);
                foreach ($data['data'] as $i => $datum) {
                    if ($datum['file']) {
                        $explode = explode('.', $datum['file']);
                        if ($explode[1] == 'jpg' || $explode[1] == 'png' || $explode[1] == 'jpeg') {
                            $data['data'][$i]['file'] = "<div class='image_preview'><img src='/uploads/" . $datum['file'] . "' alt=''></div>";
                        } else {
                            $data['data'][$i]['file'] = "<div><a target='_blank' href='/uploads/" . $datum['file'] . "'>Просмотр файла</a></div>";
                        }
                    }
                }
                return response()->json($data, 200);
                break;

            case "addBlock":
                $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'type' => 'required',
                    'file' => 'file',
                ]);

                if ($validator->fails()) {
                    $results['status'] = false;
                    $results['errors'] = $validator->errors()->all();
                    return response()->json($results, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                try {
                    $inputs = $request->except('_token', 'action');
                    if (isset($inputs['file'])) {
                        $filename = str_random(5) . date('Ymd-his') . '.' . $request['file']->getClientOriginalExtension();
                        Storage::disk('uploads')->put($filename, file_get_contents($request['file']->getRealPath()));
                        $inputs['file'] = $filename;
                    }
                    Block::create($inputs);
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }

                return response()->json($data, 200);
                break;

            case "editBlock":
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'title' => 'required',
                    'type' => 'required',
                    'file' => 'file',
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                try {
                    $inputs = $request->except('_token', 'action');
                    $block = Block::find($inputs['id']);
                    if (isset($inputs['file'])) {
                        Storage::disk('uploads')->delete($block->file);
                        $filename = str_random(5) . date('Ymd-his') . '.' . $request['file']->getClientOriginalExtension();
                        Storage::disk('uploads')->put($filename, file_get_contents($request['file']->getRealPath()));
                        $inputs['file'] = $filename;
                    }
                    Block::where('id', $request['id'])->update($inputs);
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }

                return response()->json($data, 200);
                break;
            case "deleteBlock":
                $validator = Validator::make($request->all(), [
                    'id' => 'required'
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => 'Запись успешно удалена');
                try {
                    $block = Block::find($request['id']);
                    Storage::disk('uploads')->delete($block->file);
                    $block->delete();
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }
                return response()->json($data, 200);
                break;
            default:
                break;
        }
    }
}
