<?php

namespace App\Http\Controllers\Admin;

use App\Models\Extjs;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {		
        return view('admin.panel.users.user');
    }


    public function ajax(Request $request)
    {
        $action = $request['action'];

        switch ($action) {
            case "users":
                $query = User::select(DB::raw("id, fio, email, phone, city, company, position, is_role"));
                $data = Extjs::jsQuery($query);
                return response()->json($data, 200);
                break;

            case "addUser":
                $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required|confirmed|min:6',
                    'fio' => 'required',
                    'phone' => 'required',
                    'is_role' => 'required',
                ]);

                if ($validator->fails()) {
                    $results['status'] = false;
                    $results['errors'] = $validator->errors()->all();
                    return response()->json($results, 401);
                }
                $inputs = $request->except('_token', 'password_confirmation', 'action');
                $inputs['password'] = bcrypt($inputs['password']);
                User::create($inputs);
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                return response()->json($data, 200);
                break;

            case "editUser":
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'email' => 'required',
                    'fio' => 'required',
                    'phone' => 'required',
                    'password' => 'confirmed',
                    'is_role' => 'required',
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => '');
                try {
                    $inputs = $request->except('id', '_token', 'password_confirmation', 'action', 'password');

                    if (isset($inputs['password'])) {
                        $inputs['password'] = bcrypt($inputs['password']);
                    }

                    User::where('id', $request['id'])->update($inputs);
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }

                return response()->json($data, 200);
                break;
            case "deleteUser":
                $validator = Validator::make($request->all(), [
                    'id' => 'required'
                ]);

                if ($validator->fails()) {
                    $data['status'] = false;
                    $data['errors'] = $validator->errors()->all();
                    return response()->json($data, 401);
                }
                $data = array('success' => true, 'status' => 'ok', 'msg' => 'Запись успешно удалена');
                try {
                    User::where('id', $request['id'])->delete();
                } catch (\Exception $e) {
                    $data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
                    return response()->json($data, 419);
                }
                return response()->json($data, 200);
                break;

            default:
                abort(404);
                break;
        }
    }
}
