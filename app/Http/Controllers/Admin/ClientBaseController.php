<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Extjs;

class ClientBaseController extends Controller
{
    public function index()
    {
        return view('admin.panel.client_base.client_base');
    }

    public function ajax(Request $request)
    {
        $action = $request->action;

        switch ($action) {
            case "clients":
                $query = DB::table('emails')->select(DB::raw("id, email"));
                $data = Extjs::jsQuery($query);
                return response()->json($data, 200);
                break;
            default:
                return 0;
        }
    }
}
