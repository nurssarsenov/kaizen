<?php

namespace App\Http\Controllers\Web;

use App\Models\Block;
use App\Models\Course;
use App\Models\Payment;
use App\Models\PaymentRegistrationUser;
use App\Models\RegistrationUser;
use App\Models\StaticBlock;
use App\Models\User;
use App\Models\Content;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use mysql_xdevapi\Exception;

class IndexController extends Controller
{
//    const INFO_EMAIL = 'kaizencenteralmaty@gmail.com';
	const INFO_EMAIL = 'info@kaizencenter.kz';
//    const INFO_EMAIL = 'goo.nursultan@gmail.com';
	//const INFO_EMAIL = 'kmussayev@gmail.com';
	
	public function index()
	{
		$data = [];
		$blocks = Block::where('is_active', 1)->get();
		foreach ($blocks as $block) {
			switch ($block->id) {
				case "15":
					$data['section_wrapper'] = $block;
					break;
				case "16":
					$data['section_file'] = $block;
					break;
				case "20":
					$data['section_file_2'] = $block;
					break;
				case "17":
					$data['section_information'] = $block;
					break;
				case "18":
					$data['section_start'] = $block;
					break;
				case "19":
					$data['section_courses'] = $block;
					$data['section_courses_items'] = Course::where('is_active', 1)->orderBy('start_date', 'asc')->get();
					
					$weeks = [1 => "понедельник", 2 => "вторник", 3 => "среда", 4 => "четверг", 5 => "пятница", 6 => "суббота", 7 => "воскресенье"];
					$months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
					if (count($data['section_courses_items'])) {
						foreach ($data['section_courses_items'] as $i => $section_courses_item) {
							$data['section_courses_items'][$i]['week'] = $weeks[date('N', strtotime($section_courses_item['start_date']))];
							$data['section_courses_items'][$i]['start_date'] = date('d', strtotime($section_courses_item['start_date'])) . ' ' . $months[date('m', strtotime($section_courses_item['start_date'])) - 1];
						}
					}
					break;
				case "21":
					$data['section_commands'] = $block;
					break;
				case "23":
					$data['section_gallery'] = $block;
					break;
				case "22":
					$data['section_reviews'] = $block;
					break;
			}
		}
		
		$contents = StaticBlock::where('is_active', 1)->get();
		foreach ($contents as $content) {
			switch ($content->id) {
				case "2":
					$data['requisites'] = $content;
					break;
				case "3":
					$data['address'] = $content;
					break;
				case "4":
					$data['emails'] = $content;
					break;
				case "5":
					$data['phones'] = $content;
					break;
				case "6":
					$data['map'] = $content;
					break;
			}
		}
		return view('web.index', compact('data'));
	}
	
	public function teamPage($content_id){
		$command = Content::where("id","=",$content_id)->first();
		return view('web.team-page', ['command' => $command]);
	}
	
	public function courseRegistration($courseId)
	{
		$course = Course::where('id', $courseId)->first();
		if (!$course) return abort(404);
		
		return view('web.registration', compact('course'));
	}
	
	public function courseRegistrationMembers($courseId)
	{
		$course = Course::where('id', $courseId)->first();
		if ($course) {
			$users = [];
			if (request()->token) {
				$users = RegistrationUser::join('payment_registration_users as pru', 'registration_users.id', '=', 'pru.registration_user_id')
					->join('payments as p', 'pru.payment_id', '=', 'p.id')
					->where('p.token', request()->token)
					->select('registration_users.*')
					->get();
			}
			return view('web.registration_members', compact('course', 'users'));
		}
		return abort(404);
		
	}
	
	public function registrationCourse(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'fio' => 'required',
			'company' => 'required',
			'position' => 'required',
			'city' => 'required',
			'email' => 'required|email',
			'phone' => 'required',
			'course' => 'required',
		]);
		
		if ($validator->fails()) {
			$results['status'] = false;
			$results['errors'] = $validator->errors()->all();
			return response()->json($results, 401);
		}
		try {
			$token = md5(str_random(5) . date('mdYHis'));
			$course = Course::where('id', $request['course'])->first();
			$payment = new Payment();
			$payment['token'] = $token;
			$payment['is_paid'] = 0;
			$payment['course_id'] = $course['id'];
			$payment['total_users'] = 1;
			$payment['payment_amount'] = $course['price'];
			$payment->save();
			
			$user = new RegistrationUser();
			$user['fio'] = $request['fio'];
			$user['position'] = $request['position'];
			$user['company'] = $request['company'];
			$user['phone'] = $request['phone'];
			$user['city'] = $request['city'];
			$user['email'] = $request['email'];
			$user->save();
			
			PaymentRegistrationUser::create(['payment_id' => $payment->id, 'registration_user_id' => $user->id]);
			$data['status'] = true;
			if ($course->price == 0) {
				$data['redirect'] = '/payments/success?pg_order_id=' . $payment->id . '&token=' . $token;
			} else {
				$data['redirect'] = '/payments?token=' . $token;
			}
		} catch (\Exception $e) {
			$data = array('success' => false, 'status' => 'failure', 'msg' => $e->getMessage());
			return response()->json($data, 419);
		}
		return response()->json($data, 200);
	}
	
	public function payments(Request $request)
	{
		try {
			$token = $request['token'];
			$data['courses'] = Course::join('payments as p', 'courses.id', '=', 'p.course_id')
				->where('p.token', $token)
				->select('courses.*', 'p.payment_amount', 'p.total_users', 'p.id as payment_id')
				->first();
			if ($data['courses']) {
				$data['users'] = Payment::join('payment_registration_users as pru', 'payments.id', 'pru.payment_id')
					->join('registration_users as ru', 'pru.registration_user_id', '=', 'ru.id')
					->where('payments.course_id', $data['courses']['id'])
					->where('payments.token', $token)
					->select('ru.*')
					->get();
				return view('web.payments', compact('data'));
			}
			
		} catch (\Exception $e) {
			return abort(404);
		}
		
		return abort(404);
	}


	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse|int
	 */
	public function ajax(Request $request)
	{
		$action = $request['action'];
		
		switch ($action) {
			case "paymentDeleteUser":
				$validator = Validator::make($request->all(), [
					'action' => 'required',
					'user_id' => 'required',
					'payment_id' => 'required',
				]);
				
				if ($validator->fails()) {
					$results['status'] = false;
					$results['errors'] = $validator->errors()->all();
					return response()->json($results, 401);
				}
				
				$data['status'] = true;
				$data['message'] = 'Запись успешно удалена.';
				try {
					$payment = Payment::find($request['payment_id']);
					$course = Course::find($payment->course_id);
					PaymentRegistrationUser::where('payment_id', $request['payment_id'])->where('registration_user_id', $request['user_id'])->delete();
					RegistrationUser::where('id', $request['user_id'])->delete();
					$payment->total_users = $payment->total_users - 1;
					$payment->payment_amount = $payment->payment_amount - $course->price;
					$payment->save();
					$data['html'] = view('web.payment_totals', compact('payment', 'course'))->render();
				} catch (\Exception $e) {
					$data = array('status' => false, 'message' => 'Ошибка при удалении.');
					return response()->json($data, 419);
				}
				return response()->json($data, 200);
				break;
			
			case "memberUpdate":
				$validator = Validator::make($request->all(), [
					'action' => 'required',
					'fio' => 'required',
					'position' => 'required',
					'city' => 'required',
					'email' => 'required|email',
					'phone' => 'required',
					'company' => 'required',
					'user_id' => 'required',
				]);
				
				if ($validator->fails()) {
					$results['status'] = false;
					$results['errors'] = $validator->errors()->all();
					return response()->json($results, 401);
				}
				$data['status'] = true;
				$data['message'] = 'Запись успешно сохранена.';
				try {
					
					$inputs = $request->except('user_id', 'action', '_token');
					RegistrationUser::where('id', $request['user_id'])->update($inputs);
					
				} catch (\Exception $e) {
					$data['status'] = false;
					$data['errors'][] = 'Ошибка при сохранении';
					$data['errors'][] = $e->getMessage();
					return response()->json($data, 419);
				}
				return response()->json($data, 200);
				break;
			
			case "memberDelete":
				$validator = Validator::make($request->all(), [
					'action' => 'required',
					'user_id' => 'required',
					'token' => 'required',
				]);
				
				if ($validator->fails()) {
					$results['status'] = false;
					$results['errors'] = $validator->errors()->all();
					return response()->json($results, 401);
				}
				$data['status'] = true;
				$data['message'] = 'Запись успешно удалена.';
				try {
					$payment = Payment::where('token', $request['token'])->first();
					$course = Course::find($payment->course_id);
					PaymentRegistrationUser::where('payment_id', $payment->id)->where('registration_user_id', $request['user_id'])->delete();
					RegistrationUser::where('id', $request['user_id'])->delete();
					$payment->total_users = $payment->total_users - 1;
					$payment->payment_amount = $payment->payment_amount - $course->price;
					$payment->save();
					
				} catch (\Exception $e) {
					$data['status'] = false;
					$data['errors'][] = 'Ошибка при удалении';
					$data['errors'][] = $e->getMessage();
					return response()->json($data, 419);
				}
				return response()->json($data, 200);
				break;
			
			case "addMember":
				$validator = Validator::make($request->all(), [
					'action' => 'required',
					'fio' => 'required',
					'company' => 'required',
					'position' => 'required',
					'city' => 'required',
					'email' => 'required|email',
					'phone' => 'required',
					'course' => 'required',
				]);
				if ($validator->fails()) {
					$results['status'] = false;
					$results['errors'] = $validator->errors()->all();
					return response()->json($results, 401);
				}
				try {
					$user = RegistrationUser::create([
						'fio' => $request['fio'],
						'city' => $request['city'],
						'company' => $request['company'],
						'phone' => $request['phone'],
						'position' => $request['position'],
						'email' => $request['email']
					]);
					$course = Course::find($request['course']);
					if (!empty($request['token'])) {
						$payment = Payment::where('token', '=', $request['token'])->first();
						PaymentRegistrationUser::create([
							'payment_id' => $payment['id'],
							'registration_user_id' => $user['id']
						]);
						$payment = Payment::where('id', $payment->id)->first();
						$payment->total_users = $payment->total_users + 1;
						$payment->payment_amount = $payment->payment_amount + $course->price;
						$payment->save();
					} else {
						$token = md5(str_random(5) . date('mdYHis'));
						$course = Course::where('id', $request['course'])->first();
						$payment = new Payment();
						$payment['token'] = $token;
						$payment['is_paid'] = 0;
						$payment['course_id'] = $course->id;
						$payment['total_users'] = 1;
						$payment['payment_amount'] = $course['price'];
						$payment->save();
						PaymentRegistrationUser::create(['payment_id' => $payment->id, 'registration_user_id' => $user->id]);
						$data['token'] = $token;
					}
					$data['html'] = view('web.member', compact('user'))->render();
				} catch (\Exception $e) {
					$data['status'] = false;
					$data['errors'][] = 'Ошибка при сохранении';
					$data['errors'][] = $e->getMessage();
					return response()->json($data, 419);
				}
				$data['status'] = true;
				$data['message'] = 'Запись успешно сохранена.';
				
				return response()->json($data, 200);
				break;
			
			case "saveEmail":
				$validator = Validator::make($request->all(), [
					'email' => 'required|email'
				]);
				
				if ($validator->fails()) {
					$results['status'] = false;
					$results['errors'] = $validator->errors()->all();
					return response()->json($results, 401);
				}
				try {
					$email = $request->email;
					DB::table('emails')->insert([
						'email' => $email
					]);
				} catch (\Exception $e) {
					$data['status'] = false;
					$data['errors'][] = 'Ошибка при сохранении';
					return response()->json($data, 419);
				}
				
				$data['status'] = true;
				$data['message'] = 'Запись успешно сохранена';
				
				return response()->json($data, 200);
				break;
			
			default:
				return 0;
		}
	}
	
	
	public function paymentsSuccess(Request $request)
	{
		
		$result = $request->all();
		if ($request->pg_order_id) {
			$payment = Payment::where('id', (int)$request->pg_order_id)->first();
			
			$payment->is_paid = 1;
			$payment->save();
			$course = Course::where('id', $payment->course_id)->first();
			$users = RegistrationUser::leftJoin('payment_registration_users as py', 'registration_users.id', 'py.registration_user_id')
				->leftJoin('payments as p', 'p.id', 'py.payment_id')
			    ->leftjoin('courses as c','c.id','p.course_id')
				->where('p.token', $payment->token)
				->select('registration_users.*', 'p.token','c.title','c.start_date')
				->get();
			/*send notify admin*/
			$email = self::INFO_EMAIL;
			
			Mail::send('emails.notify_registration', ['users' => $users, 'course' => $course], function ($m) use ($email) {
				$m->from('noreply@kaizencenter.kz', 'Кайдзен Центр');
				$m->to($email)->subject('Клиент прошел регистрацию');
			});
			
			Mail::send('emails.notify_registration', ['users' => $users, 'course' => $course], function ($m) use ($email) {
				$m->from('noreply@kaizencenter.kz', 'Кайдзен Центр');
				$m->to("kaizencenteralmaty@gmail.com")->subject('Клиент прошел регистрацию');
			});
			
			foreach ($users as $user) {
					Mail::send('emails.send_notify', ['user' => $user], function ($m) use ($user) {
						$m->from('noreply@kaizencenter.kz', 'Кайдзен Центр');
						$m->to($user->email, $user->fio)->subject('Приглашение на курс');
					});
				}
			
			
		}
		return view('web.payments_success', compact('result'));
	}
	
	public function sendNotify()
	{
		$myfile = fopen("testfile.txt", "w");
		
		$date = Carbon::tomorrow()->format('Y-m-d');
		try {
			$users = Course::join('payments as p', 'courses.id', '=', 'p.course_id')
				->join('payment_registration_users as pru', 'pru.payment_id', 'p.id')
				->join('registration_users as ru', 'ru.id', 'pru.registration_user_id')
				->where('courses.start_date', $date)
				->select('ru.fio', 'ru.email', 'courses.title', 'courses.start_date', 'courses.sh_content')
				->get();
			if (count($users)) {
				foreach ($users as $user) {
					Mail::send('emails.send_notify', ['user' => $user], function ($m) use ($user) {
						$m->from('noreply@kaizencenter.kz', 'Кайдзен Центр');
						$m->to($user->email, $user->fio)->subject('Приглашение на курс');
					});
				}
			}
		} catch (\Exception $e) {
			$error = $e->getMessage();
			Mail::send('emails.error', ['error' => $error], function ($m) use ($error) {
				$m->from('noreply@kaizencenter.kz', 'Кайдзен Центр');
				$m->to('goo.nursultan@gmail.com')->subject('Ошибка');
			});
		}
		return "success";
	}
}
