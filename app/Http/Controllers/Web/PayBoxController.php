<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Entity;
use App\Models\Payment;
use App\Models\RegistrationUser;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

class PayBoxController extends Controller
{
	
	//    const INFO_EMAIL = 'kaizencenteralmaty@gmail.com';
	const INFO_EMAIL = 'info@kaizencenter.kz';
//    const INFO_EMAIL = 'goo.nursultan@gmail.com';
	
	//const INFO_EMAIL = 'kmussayev@gmail.com';
	
	public function index(Request $request)
	{
		if (!empty($request['token']) && isset($request['pay'])) {
			$payment = Payment::join('courses as c', 'payments.course_id', '=', 'c.id')
				->where('token', $request['token'])
				->select('payments.id', 'payments.course_id', 'c.title as course_title', 'payments.payment_amount', 'payments.total_users')
				->first();
			if ($payment) {
				if ($request['pay'] == 1) {
					include '../app/PG_Signature.php';
					$arrReq = [
						'pg_merchant_id' => env('PAYBOX_MERCHANT_ID'),
						'pg_amount' => $payment['payment_amount'],
						'pg_currency' => 'KZT',
						'pg_description' => 'Оплата курса (' . $payment['course_title'] . ')',
						'pg_salt' => mt_rand(21, 43433),
						'pg_order_id' => $payment['id'],
						'pg_success_url' => 'http://kaizencenter.kz/payments/success?token=' . $request['token'],
						'token' => $request['token'],
						'pg_user_contact_email' => ''
					];

//                    ksort($arrReq); //sort alphabetically
//                    array_unshift($arrReq, 'payment.php');
//                    array_push($arrReq, env('PAYBOX_KEY'));
//                    $arrReq['pg_sig'] = md5(implode(';', $arrReq));
//                    unset($arrReq[0], $arrReq[1]);

					$arrReq['pg_sig'] = \PG_Signature::make('payment.php', $arrReq, env('PAYBOX_KEY'));

					$query = http_build_query($arrReq);
					$url = 'https://api.paybox.money/payment.php?' . $query;
					Log::debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@: " . $url);
					Log::debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@: " . json_encode($arrReq));
					return Redirect::to($url);
				} else {
					if (!isset($request->token)) abort(404);
					return redirect('/pay/entity?token=' . $request->token);
				}
				
			}
		}
		return abort(404);
	}
	
	public function payAtf(Request $request)
	{
		$payment = Payment::join('courses as c', 'payments.course_id', '=', 'c.id')
			->where('token', $request['token'])
			->select('payments.id', 'payments.course_id', 'c.title as course_title', 'payments.payment_amount', 'payments.total_users', 'c.content')
			->first();
		if ($payment) {
			if ($request['pay'] == 1) {
				$order = str_pad($payment->id, 6, "0", STR_PAD_LEFT);
				$amount = $payment->payment_amount;
				// $amount = 10;
				$currency = "KZT";
				$merchant = "EC711018";
				$terminal = "EC711018";
				$desc = mb_strimwidth($payment->course_title, 0, 40, "...");
				$desc_order = mb_strimwidth($payment->course_title, 0, 40, "...");
				$email = "info@kaizencenter.kz";
				$language = "ru";
				$client_id = $payment->id;
				$backref = "http://kaizencenter.kz/payments/success?redirect=true&token=" . $payment->token;
				$nonce = str_pad($payment->id, 13, "0");;
				$ucaf_Flag = "";
				$ucaf_Authentication_Data = "";
				
				$string = $order . ";" . $amount . ";" . $currency . ";" . $merchant . ";" . $terminal . ";" . $nonce . ";" . $client_id . ";" . $desc . ";" . $desc_order . ";" . $email . ";" . $backref . ";" . $ucaf_Flag . ";" . $ucaf_Authentication_Data . ";";
				$p_sign = hash("sha512", "192" . $string);
				$order = [
					"ORDER" => $order,
					"AMOUNT" => $amount,
					"CURRENCY" => $currency,
					"MERCHANT" => $merchant,
					"TERMINAL" => $terminal,
					/*optional*/
					"DESC" => $desc,
					"CLIENT_ID" => $client_id,
					"LANGUAGE" => $language,
					"DESC_ORDER" => $desc_order,
					"EMAIL" => $email,
					"BACKREF" => $backref,
					"NONCE" => $nonce,
					"Ucaf_Flag" => $ucaf_Flag,
					"Ucaf_Authentication_Data" => $ucaf_Authentication_Data,
					/*p_sign*/
					"P_SIGN" => $p_sign
				];
				$fields_string = http_build_query($order);
				return redirect('https://www.atfbank.kz/ecom/api?' . $fields_string);
			} else {
				if (!isset($request->token)) abort(404);
				return redirect('/pay/entity?token=' . $request->token);
			}
		}
		
		return redirect('/');
	}
	
	public function entity(Request $request)
	{
		$registration_user_row = RegistrationUser::LeftJoin("payment_registration_users","registration_users.id","=","payment_registration_users.registration_user_id")
										->LeftJoin("payments","payment_registration_users.payment_id","=","payments.id")
										->select("registration_users.*")
										->where("payments.token","=",$request->token)
										->first();
		$registration_user_row->is_company_user = 1;
		$registration_user_row->save(); 
		return view('web.entity',['registration_user_row' => $registration_user_row]);
	}
	
	public function ajax(Request $request)
	{
		$action = $request['action'];
		
		switch ($action) {
			case "downloadPdf":
				$inputs = $request->except('_token', 'action');
				$inputs['date'] = date('Y-m-d');
				$entity = Entity::firstOrNew(array('payments_token' => $inputs['payments_token']));
				$entity->bin_iin = $inputs['bin_iin'];
				$entity->company = $inputs['company'];
				$entity->address = $inputs['address'];
				$entity->date = $inputs['date'];
				$entity->save();
				
				$result['status'] = true;
				$result['redirect'] = '/pay/entity/ajax?action=generate&entity=' . $entity->id;
				return response()->json($result);
				break;
			case "generate":
				$data = Entity::where('id', $request->entity)->first();
				if (isset($data)) {
					$payment = Payment::where('payments.token', $data->payments_token)
						->leftJoin('courses as c', 'payments.course_id', '=', 'c.id')
						->select('payments.*', 'c.title', 'c.price')
						->first();
					$payment->is_paid = 1;
					$payment->save();
					$course = Course::where('id', $payment->course_id)->first();
					$users = RegistrationUser::leftJoin('payment_registration_users as py', 'registration_users.id', 'py.registration_user_id')
						->leftJoin('payments as p', 'p.id', 'py.payment_id')
						->leftjoin('courses as c','c.id','p.course_id')
						->where('p.token', $payment->token)
						->select('registration_users.*', 'p.token','c.title','c.start_date')
						->get();
					/*send notify admin*/
					$email = self::INFO_EMAIL;
					Mail::send('emails.notify_registration', ['users' => $users, 'course' => $course], function ($m) use ($email) {
						$m->from('noreply@kaizencenter.kz', 'Кайдзен Центр');
						$m->to($email)->subject('Клиент прошел регистрацию');
					});
					Mail::send('emails.notify_registration', ['users' => $users, 'course' => $course], function ($m) use ($email) {
						$m->from('noreply@kaizencenter.kz', 'Кайдзен Центр');
						$m->to("kaizencenteralmaty@gmail.com")->subject('Клиент прошел регистрацию');
					});
					$pdf = PDF::loadView('web.pdf', ['data' => $data, 'payment' => $payment]);
					return $pdf->download('cчет_на_оплату.pdf');
				}
				abort(404);
				break;
			
			case "generate-akt":
				$data = Entity::where('id', $request->entity)->first();
				if (isset($data)) {
					$payment = Payment::where('payments.token', $data->token)
						->leftJoin('courses as c', 'payments.course_id', '=', 'c.id')
						->select('payments.*', 'c.title', 'c.price')
						->first();
					Excel::create('akt', function ($excel) use ($data, $payment) {
						$excel->sheet('New sheet', function ($sheet) use ($data, $payment) {
							$sheet->getStyle('C8:O8')->getAlignment()->setWrapText(true);
							$sheet->getStyle('C10:O10')->getAlignment()->setWrapText(true);
							$sheet->setHeight(8, 50);
							$sheet->setHeight(10, 50);
							$sheet->loadView('web.excel-akt', ['data' => $data, 'payment' => $payment]);
							
						});
					})->download('xlsx');
				}
				abort(404);
				break;
		}
	}
	
	
}
