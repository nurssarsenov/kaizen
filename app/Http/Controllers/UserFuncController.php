<?php

namespace App\Http\Controllers;

use App\Models\RegistrationUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserFuncController extends Controller
{
    public function showOrdersHistory(){
        if(!Auth::check())return redirect('/');
        if(!auth()->user()->is_email_verified) return view('auth.verify');

        $registrationUsers = RegistrationUser::where("email", auth()->user()->email)
            ->join('payment_registration_users as pru', 'registration_users.id', 'pru.registration_user_id')
            ->join('payments as p', 'pru.payment_id', '=', 'p.id')
            ->join('courses as c', 'p.course_id', '=', 'c.id')
            ->leftJoin('entities as e', 'p.token', '=', 'e.payments_token')
            //->where('p.is_paid', 1)
            ->select(DB::raw("e.id as entities_id, p.token , p.id as payment_id,registration_users.is_company_user, registration_users.created_at, registration_users.position, registration_users.id, registration_users.fio, registration_users.email, registration_users.phone, registration_users.city, registration_users.company,p.is_paid, c.title  course_title, c.price, c.id as course_id , c.start_date"))
            ->paginate(15);

        return view('web.order-history', ['registrationUsers' => $registrationUsers]);
    }

    public function userLogin(){
        return view('admin.auth.user-login');
    }
}
