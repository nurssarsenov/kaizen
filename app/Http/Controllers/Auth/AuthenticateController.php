<?php

namespace App\Http\Controllers\Auth;

use App\Mail\ResetPasswordMail;
use App\Models\ResetPassword;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use ResetPasswords;

class AuthenticateController extends Controller
{
    public function login()
    {
        if (!Auth::check()) {
            return view('admin.auth.login');
        }
        return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function ajax(Request $request)
    {
        $action = $request['action'];
        switch ($action) {
            case "authorization":
                $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required|min:6'
                ]);
                if ($validator->fails()) {
                    $results['status'] = false;
                    $results['errors'] = $validator->errors()->all();
                    return response()->json($results, 401);
                }
                $results['status'] = false;
                $results['errors'][] = 'Неверный email или пароль';
                $credentials = [
                    'email' => $request['email'],
                    'password' => $request['password'],
                ];

                if (Auth::attempt($credentials)) {
                    $results['status'] = true;
                    $results['redirect'] = '/admin';
                    return response()->json($results, 200);
                }
                return response()->json($results, 401);
                break;
        }
    }

    public function resetPasswordView()
    {
        if (!Auth::check()) {
            return view('admin.auth.password-reset');
        }
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);


        if ($validator->fails()) {
            $results['status'] = false;
            $results['errors'] = $validator->errors()->all();
            return response()->json($results, 401);
        }
        try {
            $users = User::where("email", $request->get('email'))->get();
            if (!count($users)) {
                $data = array('success' => false, 'status' => false, 'errors' => ["Аккаунт с таким E-mail не найден!"]);
                return response()->json($data, 419);
            }
            $user = $users->first();
            $passwordReset = ResetPassword::where('user_id', $user->id)->get()->first();
            Log::debug("Password reset is: " . $passwordReset);
            if (!$passwordReset) {
                $passwordReset = ResetPassword::create([
                    'user_id' => $user->id,
                    'token' => str_random(40)
                ]);
            }else{
                $passwordReset->is_used = 0;
                $passwordReset->token=str_random(40);
                $passwordReset->save();
            }
            Mail::to($user->email, ['user' => $user])->send(new ResetPasswordMail($user));

        } catch (\Exception $e) {
            $data = array('success' => false, 'status' => false, 'errors' => ["Что-то пошло не так"]);
            return response()->json($data, 419);
        }
        $data = array('success' => true, 'status' => true, 'msgs' => ["Check your email"], 'redirect' => '/userLogin');
        return response()->json($data, 200);
    }

    public function allowResetPassword($token)
    {
        Log::debug("TOKTKEKKEKEKEN  " . $token);
        $passwordReset = ResetPassword::where('token', $token)->first();
        Log::debug("asdasd____ " . $passwordReset);
        if (isset($passwordReset)) {
            $user = $passwordReset->user;

            if ($passwordReset->is_used) {
                $status = "Ваш E-mail ранее уже был активирован. Войдите.";
                return redirect('/userLogin')->with('warning', "Sorry your email cannot be reset.");
            }
            $passwordReset->is_used = 1;
            $passwordReset->save();
            $status = "Поздравляем, Вы можете восстановить аккаунт введя новый пароль.";
            return view('web.new-password');
        } else {
            return redirect(route('userLogin'))->with('warning', "Ссылка доступна только на один раз. ");
        }

    }

    public function changePassword(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'password2' => 'required|same:password',
            'url' => 'required|url'
        ]);


        if ($validator->fails()) {
            $results['status'] = false;
            $results['errors'] = $validator->errors()->all();
            return response()->json($results, 401);
        }
        $token = substr(strrchr($request->get('url'), '/'),1);
        Log::debug("Token: " . $token);
        try {
            $user = User::join('reset_passwords as rp','rp.user_id','=','users.id')->where("token", $token)->get()->first();
            Log::debug("User: " . $user);
            $user = User::where('id', $user->user_id)->get()->first();
            Log::debug("User: " . $user);
            $user->password = bcrypt($request->get('password'));
            $user->save();
            $data['status'] = true;
            $data['redirect'] = '/userLogin';
        } catch (\Exception $e) {
            $data = array('success' => false, 'status' => 'failure', 'errors' => "Что пошло не так", 'msg' => $e->getMessage());
            Log::debug($e->getMessage());
            return response()->json($data, 419);
        }
        $data = array('success' => true, 'status' => true, 'msgs' => ["Пароль успешно изменен! Войдите под новым паролем"], 'redirect' => '/userLogin');
        return response()->json($data, 200);
    }
}
