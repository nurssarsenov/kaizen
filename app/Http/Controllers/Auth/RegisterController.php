<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\VerifyMail;
use App\Models\Course;
use App\Models\Payment;
use App\Models\PaymentRegistrationUser;
use App\Models\RegistrationUser;
use App\Models\User;
use App\VerifyUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function index(){
        if(Auth::check())return redirect('/');
        return view('admin.auth.register');
    }


    public function registerUser(Request $request){
        $validator = Validator::make($request->all(), [
            'fio' => 'required',
            'company' => 'required',
            'position' => 'required',
            'city' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'password' => 'required|min:6',
            'password2' => 'required|same:password',
        ]);


        if ($validator->fails()) {
            $results['status'] = false;
            $results['errors'] = $validator->errors()->all();
            return response()->json($results, 401);
        }
        try {
            if(count(User::where("email" , $request->get('email'))->get())) {
                $data = array('success' => false, 'status' => false, 'errors' => ["Аккаунт с таким mail уже зарегистрировался"]) ;
                return response()->json($data, 419);
            }
            Log::debug("-_-_-_-_-_-_-_---___---_-_-_____1____");
            $user = new User();
            $user->fio=$request['fio'];
            $user->position= $request['position'];
            $user->city=$request['city'];
            $user->email= $request['email'];
            $user->password=bcrypt($request['password']);
            $user->phone= $request['phone'];
            $user->company=$request['company'];
            $user->is_role=0;
            $user->is_email_verified=0;
            $user->save();
            Log::debug("-_-_-_-_-_-_-_---___---_-_-_____2____");
            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => str_random(40)
            ]);
            if(!$verifyUser){
                $user->delete();
            }

            Log::debug("asda_-__-___----s");
            Mail::to($user->email, ['user' => $user])->send(new VerifyMail($user));
        } catch (\Exception $e) {
            $data = array('success' => false, 'status' => 'failure','errors' =>"Что пошло не так", 'msg' => $e->getMessage());
            Log::debug($e->getMessage());
            return response()->json($data, 419);
        }
        $data = array('success' => true, 'status' => true, 'msgs' => ["Проверьте почту, что бы активировать аккаунт. \n Вы будете перенаправлены через 5 сек"], 'redirect' => '/userLogin');
        return response()->json($data, 200);
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->is_email_verified) {
                $verifyUser->user->is_email_verified = 1;
                $verifyUser->user->save();
                $status = "Поздравляем, Ваш e-mail активирован. Теперь вы можете смотреть ваши истории заказов.";
            }else{
                $status = "Ваш E-mail ранее уже был активирован. Войдите.";
            }
        }else{
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }

        return redirect(route('userLogin'))->with('status', $status);
    }

    public function resendVerification(){
        Mail::to(auth()->user()->email)->send(new VerifyMail(auth()->user()));
        session_start();
        $_SESSION['isResendVerification'] = true;
        return redirect('/history');
    }
}
